[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

# DOTFILES

My dotfiles for NixOS.

I made the background image, [feel free to use it](https://bg.kylesferrazza.com/bg.png).

# Setup

## Setup channel (for normal nix-shell)

This is _not_ necessary when just rebuilding, but will be used by any nix expressions (like those in project-level `shell.nix` files) that call `import <nixpkgs>`.

This will also fix `command-not-found`.

```bash
sudo nix-channel --add https://nixos.org/channels/nixos-unstable nixos
sudo nix-channel --update
```

## Clone repo and rebuild

```bash
sudo chown kyle: /etc/nixos
git clone --recursive https://gitlab.com/kylesferrazza/nix.git /etc/nixos
cd /etc/nixos
sudo nixos-rebuild --flake . switch
```
