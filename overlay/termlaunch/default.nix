{ writeShellScriptBin, kitty }:
writeShellScriptBin "termlaunch" ''
  kitty /bin/sh -c "$@"
''
