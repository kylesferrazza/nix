{
  writeShellScriptBin,
  bash,
  coreutils,
  findutils,
}:
writeShellScriptBin "xargs-each" ''
  ${coreutils}/bin/tr '\n' '\0' | ${findutils}/bin/xargs -0 -n 1 $@
''
