{ masterPkgs }:
final: prev:
let
  inherit (prev) callPackage;
  termlaunch = callPackage ./termlaunch { };
in
{
  inherit (masterPkgs) jujutsu vivaldi;

  mine = {
    inherit termlaunch;

    dotscrot = callPackage ./dotscrot { inherit termlaunch; };
    evim = callPackage ./evim { inherit termlaunch; };
    isp = callPackage ./isp { };
    xargs-each = callPackage ./xargs-each { };
    ente-photos-desktop = callPackage ./ente.nix { };
  };
}
