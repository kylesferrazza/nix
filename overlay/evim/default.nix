{
  writeShellScriptBin,
  wmctrl,
  gawk,
  findutils,
  i3-gaps,
  xdotool,
  termlaunch,
}:
writeShellScriptBin "evim" ''
  file=$(mktemp)
  ${termlaunch}/bin/termlaunch "nvim $file" &
  sleep 0.5
  i3-msg floating enable
  i3-msg resize set 800 400
  wait
  echo -e \'$(cat $file)\' | ${findutils}/bin/xargs ${xdotool}/bin/xdotool type
  rm $file
''
