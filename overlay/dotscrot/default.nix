{
  writeShellScript,
  writeShellScriptBin,
  bash,
  termlaunch,
  neofetch,
  maim,
  procps,
  chromium,
  htop,
}:
let
  runHang = writeShellScript "runHang" ''
    ${termlaunch}/bin/termlaunch "sleep 3 && $@ && read"
  '';
in
writeShellScriptBin "dotscrot" ''
  SHOTDIR=$(pwd)

  cd /etc/nixos
  i3-msg workspace 9
  $TERMINAL &
  emacsclient -c configuration.nix &
  sleep 1

  ${runHang} ${neofetch}/bin/neofetch &
  sleep 1
  i3-msg 'floating enable'
  i3-msg 'move position center'
  i3-msg 'resize set 900 500'
  sleep 5

  ${maim}/bin/maim -u "$SHOTDIR/shot1.png"

  sleep 1
  ${procps}/bin/pkill -P $$

  ${maim}/bin/maim -u "$SHOTDIR/shot2.png"

  sleep 1
  ${procps}/bin/pkill -P $$

  ${chromium}/bin/chromium https://gitlab.com/kylesferrazza/nix/commit/cbe170db9dfaf9d413e0f26079e129ce416efe44 &
  ${runHang} ${htop}/bin/htop &
  sleep 8
  ${maim}/bin/maim -u "$SHOTDIR/shot3.png"

  sleep 1
  ${procps}/bin/pkill -P $$

  i3-msg workspace 1
''
