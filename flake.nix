{
  description = "Kyle Sferrazza's nix configs.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-master.url = "github:NixOS/nixpkgs/master";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    darwin = {
      url = "github:lnl7/nix-darwin/nix-darwin-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    doom-emacs.url = "gitlab:kylesferrazza/doom-d";
    spicetify-nix = {
      url = "gitlab:kylesferrazza/spicetify-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    deadnix = {
      url = "github:astro/deadnix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    neovim-config = {
      url = "gitlab:kylesferrazza/neovim-config";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    hyprland = {
      type = "git";
      url = "https://github.com/hyprwm/hyprland";
      ref = "refs/tags/v0.47.2";
      submodules = true;
    };
    nur.url = "github:nix-community/NUR";
    vscode-server.url = "github:nix-community/nixos-vscode-server";
    nixos-wsl.url = "github:nix-community/NixOS-WSL";
  };

  outputs =
    inputs@{
      self,
      nixpkgs,
      nixpkgs-master,
      home-manager,
      doom-emacs,
      spicetify-nix,
      deadnix,
      neovim-config,
      hyprland,
      nur,
      vscode-server,
      nixos-wsl,
      darwin,
    }:
    let
      homeModulesLst = {
        helix = import ./homeModules/helix;
        jujutsu = import ./homeModules/jujutsu;
      };
      mkOverlay =
        system:
        import ./overlay {
          masterPkgs = import nixpkgs-master {
            inherit system;
            config = {
              allowUnfree = true;
            };
          };
        };
      stablePkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ (mkOverlay "x86_64-linux") ];
        config.allowUnfree = true;
      };
      mkHome =
        nixos-config:
        home-manager.lib.homeManagerConfiguration {
          pkgs = stablePkgs;
          modules = [
            {
              home = {
                username = "kyle";
                homeDirectory = "/home/kyle";
              };
            }
            nixos-config.config.home-manager.users.${nixos-config.config.kyle.username}
            { imports = nixpkgs.lib.attrValues homeModulesLst; }
          ];
        };
    in
    rec {
      homeConfigurations = {
        "kyle@nyx" = mkHome nixosConfigurations.nyx;
        "kyle@zagreus" = mkHome nixosConfigurations.zagreus;
      };
      darwinConfigurations."mercury" = darwin.lib.darwinSystem {
        system = "aarch64-darwin";
        specialArgs = {
          spicetify-nix = spicetify-nix.packages."aarch64-darwin";
        };
        modules = [
          ./to-merge/darwin/configuration.nix
          { nixpkgs.overlays = [ (mkOverlay "aarch64-darwin") ]; }
          home-manager.darwinModules.home-manager
          { environment.systemPackages = [ neovim-config.defaultPackage.aarch64-darwin ]; }
          { imports = nixpkgs.lib.attrValues darwinModules; }
          { home-manager.sharedModules = nixpkgs.lib.attrValues homeModulesLst; }
        ];
      };
      nixosConfigurations =
        let
          system-config =
            name: system:
            let
              myoverlay = mkOverlay system;
            in
            {
              "${name}" = nixpkgs.lib.makeOverridable nixpkgs.lib.nixosSystem {
                inherit system;
                specialArgs = {
                  inherit doom-emacs;
                  util = stablePkgs.callPackage ./util { };
                  extras = {
                    inherit
                      spicetify-nix
                      neovim-config
                      hyprland
                      ;
                  };
                };
                modules = [
                  vscode-server.nixosModules.default
                  nur.modules.nixos.default
                  {
                    nixpkgs.overlays = [ myoverlay ];
                    nixpkgs.config.allowUnfree = true;
                    nix.registry.nixpkgs.flake = nixpkgs;
                  }
                  home-manager.nixosModules.home-manager
                  { home-manager.sharedModules = nixpkgs.lib.attrValues homeModulesLst; }
                  { home-manager.useGlobalPkgs = true; }
                  { imports = nixpkgs.lib.attrValues nixosModules; }
                  (import (./hosts + "/${name}/default.nix"))
                  (import (./hosts + "/${name}/hardware-configuration.nix"))
                ];
              };
            };
        in
        { }
        // (system-config "nyx" "x86_64-linux")
        // (system-config "zagreus" "x86_64-linux")
        // (system-config "mars" "x86_64-linux")
        // {
          ares-wsl = nixpkgs.lib.nixosSystem {
            system = "x86_64-linux";
            specialArgs = {
              inherit nixos-wsl neovim-config;
            };
            modules = [
              { nixpkgs.overlays = [ (mkOverlay "x86_64-linux") ]; }
              home-manager.nixosModules.home-manager
              { home-manager.sharedModules = nixpkgs.lib.attrValues homeModulesLst; }
              ./to-merge/wsl/configuration.nix
              nixosModules.username
            ];
          };
        };

      darwinModules = {
        username = import ./modules/username.nix;
        colors = import ./modules/colors;
        kitty = import ./modules/kitty.nix;
        starship = import ./modules/starship.nix;
        dock = import ./modules/macDock.nix;
      };
      homeModules = homeModulesLst;
      nixosModules = {
        username = import ./modules/username.nix;
        efi = import ./modules/efi.nix;
        git = import ./modules/git.nix;
        hidpi = import ./modules/hidpi.nix;
        i3 = import ./modules/i3;
        neovim = import ./modules/neovim.nix;
        pxe = import ./modules/pxe.nix;
        polybar = import ./modules/polybar;
        virtualisation = import ./modules/virtualisation.nix;
        dunst = import ./modules/dunst.nix;
        gammastep = import ./modules/gammastep.nix;
        rofi = import ./modules/rofi.nix;
        sshd = import ./modules/sshd.nix;
        colors = import ./modules/colors;
        gtk = import ./modules/gtk;
        kitty = import ./modules/kitty.nix;
        xorg = import ./modules/xorg;
        hyprland = import ./modules/hyprland;
        nvidia = import ./modules/nvidia.nix;
        wayland = import ./modules/wayland;
        browser = import ./modules/browser;
        tailscale = import ./modules/tailscale.nix;
        bluetooth = import ./modules/bluetooth.nix;
        starship = import ./modules/starship.nix;
      };

      packages.x86_64-linux = (overlay stablePkgs stablePkgs).mine // rec {
        spotify-info = stablePkgs.callPackage ./modules/polybar/spotify-info.nix { };
        spotify-copy = stablePkgs.callPackage ./modules/polybar/spotify-copy.nix { inherit spotify-info; };
      };
      devShell.x86_64-linux = stablePkgs.mkShell {
        name = "nix";
        buildInputs = [
          deadnix.packages.x86_64-linux.deadnix
          nixpkgs.legacyPackages.x86_64-linux.lefthook
          nixpkgs.legacyPackages.x86_64-linux.nixfmt-rfc-style
        ];
        shellHook = ''
          lefthook install
        '';
      };

      overlay = mkOverlay "x86_64-linux";

      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixfmt-rfc-style;
      formatter.aarch64-darwin = nixpkgs.legacyPackages.aarch64-darwin.nixfmt-rfc-style;
    };
}
