{ pkgs, lib }:
let
  faVer = lib.last (lib.strings.split "-" pkgs.font-awesome.name);
  faMajor = lib.head (lib.strings.split "\\." faVer);
in
{
  # TODO remove util altogether
  bg = import ./bg.nix;
  fontAwesomeVersion = faMajor;
}
