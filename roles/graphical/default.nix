{ lib, ... }:
{
  # TODO: roles becomes modules that enable other modules

  kyle = {
    hyprland.enable = true;
    virtualisation.enable = true;
    colors.solarized-dark.enable = true;
    gtk.solarized-dark.enable = true;
    terminal.kitty.enable = true;
    # browser.firefox.enable = true;
    # browser.chromium.enable = true;
    browser.vivaldi.enable = true;
  };

  # specialisation.light-mode.configuration = {
  #   kyle.colors = {
  #     solarized-dark.enable = lib.mkForce false;
  #     solarized-light.enable = true;
  #   };
  #   kyle.gtk = {
  #     solarized-dark.enable = lib.mkForce false;
  #     solarized-light.enable = true;
  #   };
  # };

  # specialisation.i3.configuration.kyle = {
  #   hyprland.enable = lib.mkForce false;
  #   i3.enable = true;
  # };

  imports = [
    ./programs
    ./fonts.nix
    ./printing.nix
    ./idea.nix
    ./gimp.nix
    ./meld.nix
    ./qt.nix
  ];
}
