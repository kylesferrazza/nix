{ pkgs, config, ... }:
{
  home-manager.users.${config.kyle.username} = {
    home.packages = with pkgs; [ gimp ];

    xdg.configFile."GIMP/2.10/themerc".text = ''
      include "${pkgs.gimp}/share/gimp/2.0/themes/System/gtkrc"
      include "${pkgs.gimp}/etc/gimp/2.0/gtkrc"
    '';
  };
}
