{ config, ... }:
{
  home-manager.users.${config.kyle.username}.home.file.".ideavimrc" = {
    text = ''
      ino jk <esc>
      set clipboard=unnamedplus
      nno <C-S> :s/\v\((.*), (.*)\);$/(\2, \1);/<CR>
      set surround
    '';
  };
}
