{ config, ... }:
{
  home-manager.users.${config.kyle.username}.qt = {
    enable = true;
    platformTheme.name = "gtk";
  };
}
