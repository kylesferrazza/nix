{ pkgs, ... }:
{
  # hardware.sane.enable = true;
  # environment.systemPackages = with pkgs; [ gscan2pdf ];

  services = {
    printing = {
      enable = true;
      drivers = with pkgs; [ hplip ];
    };
  };
}
