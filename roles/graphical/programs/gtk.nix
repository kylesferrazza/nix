{
  config,
  pkgs,
  util,
  ...
}:
{
  environment.systemPackages = with pkgs; [
    dconf # some GTK apps break without this package
  ];

  services.gnome.at-spi2-core.enable = true; # many apps complain when this isn't running

  home-manager.users.kyle = {
    gtk = {
      enable = true;
      iconTheme = config.kyle.gtk.icons;
      theme = config.kyle.gtk.theme;
    };

    xdg.configFile."gtk-3.0/bookmarks".text = ''
      file:///home/kyle/Documents Documents
      file:///home/kyle/Downloads Downloads
      file:///home/kyle/Sync Sync
    '';
  };
}
