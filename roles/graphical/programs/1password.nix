{ pkgs, config, ... }:
{
  programs.ssh.extraConfig = ''
    Host *
      IdentityAgent ~/.1password/agent.sock
  '';
  programs._1password-gui = {
    enable = true;
    polkitPolicyOwners = [ "kyle" ];
  };
  programs._1password.enable = true;

  home-manager.users.${config.kyle.username} = {
    home.sessionVariables = {
      SSH_AUTH_SOCK = "/home/kyle/.1password/agent.sock";
    };
    home.file.".config/1Password/ssh/agent.toml".text = ''
      [[ssh-keys]]
      item = "gitlab"
      vault = "Private"

      [[ssh-keys]]
      item = "commit-signing"
      vault = "Private"

      [[ssh-keys]]
      item = "github"
      vault = "Private"

      [[ssh-keys]]
      item = "vm"
      vault = "Private"
    '';
  };
}
