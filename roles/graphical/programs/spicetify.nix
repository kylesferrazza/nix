{ pkgs, extras, ... }:
{
  environment.systemPackages = [ extras.spicetify-nix.packages.x86_64-linux.solarizedDark ];
}
