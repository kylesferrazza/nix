{ pkgs, ... }:
{
  networking.networkmanager.enable = true;
  users.users.kyle.extraGroups = [ "networkmanager" ];
  services.gnome.gnome-keyring.enable = true;
}
