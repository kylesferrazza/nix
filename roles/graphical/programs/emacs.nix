{
  config,
  pkgs,
  lib,
  doom-emacs,
  ...
}:
{

  home-manager.users.${config.kyle.username} = {
    home.file.".emacs.d/init.el".text = ''
      (load "default.el")
    '';

    home.packages = with pkgs; [ emacs-all-the-icons-fonts ];

    programs.emacs = {
      enable = true;
      package = doom-emacs.defaultPackage.x86_64-linux;
    };
    services.emacs.enable = true;
    xsession.windowManager.i3.config.keybindings =
      let
        modifier =
          config.home-manager.users.${config.kyle.username}.xsession.windowManager.i3.config.modifier;
      in
      {
        "${modifier}+Insert" = "exec ${doom-emacs}/bin/emacsclient -c";
      };
  };
}
