{ pkgs, config, ... }:
{
  environment.systemPackages = with pkgs; [ vscode ];

  home-manager.users.${config.kyle.username}.home.file.".vscode/argv.json".text = ''
    {
      "enable-crash-reporter": false,
      "password-store": "gnome"
    }
  '';
}
