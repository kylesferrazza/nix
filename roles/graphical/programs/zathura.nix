{ config, ... }:
{
  home-manager.users.${config.kyle.username}.programs.zathura = {
    enable = true;
    options = {
      notification-error-bg = "#586e75";
      notification-error-fg = "#dc322f";
      notification-warning-bg = "#586e75";
      notification-warning-fg = "#dc322f";
      notification-bg = "#586e75";
      notification-fg = "#b58900";
      completion-group-bg = "#002b36";
      completion-group-fg = "#839496";
      completion-bg = "#073642";
      completion-fg = "#93a1a1";
      completion-highlight-bg = "#586e75";
      completion-highlight-fg = "#eee8d5";
      index-bg = "#073642";
      index-fg = "#93a1a1";
      index-active-bg = "#586e75";
      index-active-fg = "#eee8d5";
      inputbar-bg = "#586e75";
      inputbar-fg = "#eee8d5";
      statusbar-bg = "#073642";
      statusbar-fg = "#93a1a1";
      highlight-color = "#657b83";
      highlight-active-color = "#268bd2";
      highlight-transparency = "0.2";
      default-bg = "#073642";
      default-fg = "#93a1a1";
      recolor = false;
      recolor-lightcolor = "#073642";
      recolor-darkcolor = "#93a1a1";
    };
  };
}
