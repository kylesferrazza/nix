{ pkgs, ... }:
{
  imports = [
    ./adb.nix
    ./docker.nix
    ./gtk.nix
    ./kdenlive.nix
    ./light.nix
    ./morse.nix
    ./networkmanager.nix
    ./pentest
    ./plymouth.nix
    ./steam.nix
    ./yubikey.nix
    # ./emacs.nix
    # ./spicetify.nix
    ./zathura.nix
    ./syncthing.nix
    ./vscode.nix
    ./vscodeserver.nix
    ./1password.nix
  ];

  environment.systemPackages = with pkgs; [
    inkscape
    feh
    discord
    signal-desktop
    slack
    gparted
    jdiskreport
    remmina
    virt-viewer
    vlc
    mine.dotscrot
    # teams
    element-desktop
    # multimc
    zoom-us
    todoist-electron
    thunderbird
    obsidian

    # cloudflare-warp
    # mine.ente-photos-desktop
  ];

  services.usbmuxd.enable = true;
  documentation.dev.enable = true;
}
