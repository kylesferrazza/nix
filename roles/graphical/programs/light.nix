{ pkgs, ... }:
{
  programs.light.enable = true;
  users.users.kyle.extraGroups = [ "video" ];
}
