{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    kdenlive
    frei0r
    ffmpeg-full
  ];
}
