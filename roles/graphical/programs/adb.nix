{ ... }:
{
  programs.adb.enable = true;
  users.users.kyle.extraGroups = [ "adbusers" ];
}
