{ ... }:
{
  services.syncthing = {
    enable = true;
    user = "kyle";
    group = "users";
    openDefaultPorts = true;
    dataDir = "/home/kyle/Sync";
  };
}
