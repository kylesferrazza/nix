{ pkgs, config, ... }:
{
  home-manager.users.${config.kyle.username} = {
    home.packages = with pkgs; [ meld ];

    home.sessionVariables.DIFFPROG = "meld";

    programs.git.extraConfig = {
      diff = {
        tool = "meld";
      };
      difftool = {
        prompt = "false";
      };
      merge = {
        tool = "meld";
        conflictstyle = "diff3";
      };
      mergetool = {
        prompt = "false";
      };
    };
  };
}
