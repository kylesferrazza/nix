{ util, pkgs, ... }:
{
  fonts = {
    enableDefaultPackages = true;
    fontDir.enable = true;
    enableGhostscriptFonts = true;
    packages = with pkgs; [
      # corefonts
      dejavu_fonts
      noto-fonts
      fira-code
      iosevka
    ];

    fontconfig = {
      defaultFonts = {
        serif = [
          "DejaVu Serif"
          "Font Awesome ${util.fontAwesomeVersion} Free"
        ];
        sansSerif = [
          "DejaVu Sans"
          "Font Awesome ${util.fontAwesomeVersion} Free"
        ];
        monospace = [
          "Iosevka"
          "Font Awesome ${util.fontAwesomeVersion} Free"
        ];
        emoji = [ "Noto Color Emoji" ];
      };
    };
  };
  home-manager.users.kyle = {
    fonts.fontconfig.enable = true;

    home.packages = with pkgs; [
      fira-code
      font-awesome
      iosevka
    ];
  };
}
