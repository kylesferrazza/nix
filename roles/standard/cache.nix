{ ... }:
{
  nix.settings = {
    substituters = [ "https://kylesferrazza-zagreus.cachix.org" ];
    trusted-public-keys = [
      "kylesferrazza-zagreus.cachix.org-1:bpOhAQvR0y04RRB1U4DyS8B8oMq7PTq6lf1LY7ibv9c="
    ];
  };
}
