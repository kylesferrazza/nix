{ pkgs, config, ... }:
{
  home-manager.users.${config.kyle.username} = {
    programs.bash.initExtra = ''
      source ${pkgs.fzf}/share/fzf/key-bindings.bash
      source ${pkgs.fzf}/share/fzf/completion.bash
    '';

    programs.fish.interactiveShellInit = ''
      source ${pkgs.fzf}/share/fzf/key-bindings.fish
      fzf_key_bindings
    '';
  };
}
