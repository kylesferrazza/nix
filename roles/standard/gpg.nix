{ pkgs, config, ... }:
{
  home-manager.users.${config.kyle.username} = {
    home.packages = with pkgs; [ gnupg ];

    services.gpg-agent = {
      enable = true;
      defaultCacheTtl = 1800;
      enableSshSupport = true;
      extraConfig = ''
        pinentry-program ${pkgs.pinentry-gtk2}/bin/pinentry
      '';
    };
  };
}
