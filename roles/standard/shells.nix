{ pkgs, config, ... }:
let
  clbin = pkgs.writeShellScriptBin "clbin" ''
    cat "$@" | curl -sfF "clbin=<-" https://clbin.com
  '';
  aliases = {
    pb = "${clbin}/bin/clbin";
    clip = "${pkgs.xclip}/bin/xclip -selection c";
    nix-fish = "nix-shell --run fish";
  };
in
{
  home-manager.users.${config.kyle.username} = {
    programs.bash = {
      enable = true;
      shellAliases = aliases;
      initExtra = ''
        if [ -n "$IN_NIX_SHELL" ]; then
          export TERMINFO=/run/current-system/sw/share/terminfo
        fi
      '';
    };
    programs.fish = {
      enable = true;
      shellAliases = aliases;
      interactiveShellInit = ''
        set fish_greeting

        set -l base03  "--bold black"
        set -l base02  "black"
        set -l base01  "--bold green"
        set -l base00  "--bold yellow"
        set -l base0   "--bold blue"
        set -l base1   "--bold cyan"
        set -l base2   "white"
        set -l base3   "--bold white"
        set -l yellow  "yellow"
        set -l orange  "--bold red"
        set -l red     "red"
        set -l magenta "magenta"
        set -l violet  "--bold magenta"
        set -l blue    "blue"
        set -l cyan    "cyan"
        set -l green   "green"

        set -g fish_color_normal      $base0
        set -g fish_color_command     $base0
        set -g fish_color_quote       $cyan
        set -g fish_color_redirection $base0
        set -g fish_color_end         $base0
        set -g fish_color_error       $red
        set -g fish_color_param       $blue
        set -g fish_color_comment     $base01
        set -g fish_color_match       $cyan
        set -g fish_color_search_match "--background=$base02"
        set -g fish_color_operator    $orange
        set -g fish_color_escape      $cyan

        # fish_vi_key_bindings
        # bind -M insert -m default jk backward-char force-repaint
      '';
    };
  };
}
