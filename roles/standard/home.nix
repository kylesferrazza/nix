{ pkgs, config, ... }:
{
  home-manager.backupFileExtension = "hmbak";
  home-manager.useUserPackages = true;

  systemd.services.home-manager-kyle.preStart = ''
    # fixes build-vm for home-manager
    # https://github.com/rycee/home-manager/issues/948
    ${pkgs.nix}/bin/nix-env -i -E
  '';

  home-manager.users.${config.kyle.username} = {
    home.stateVersion = "24.05";
    home.extraOutputsToInstall = [
      "doc"
      "info"
      "devdoc"
    ];
    services = {
      pasystray.enable = true;
      network-manager-applet.enable = true;
      lorri.enable = true;
    };
    programs.command-not-found.enable = true;
    kyle = {
      helix = {
        enable = true;
        default = true;
      };
      jujutsu.enable = true;
    };
  };
}
