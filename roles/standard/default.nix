{
  lib,
  config,
  pkgs,
  options,
  ...
}:
{
  imports = [
    ./fhs
    ./cache.nix
    # ./avahi.nix
    ./aur.nix
    ./pkgs.nix
    ./home.nix
    ./direnv.nix
    ./haskell.nix
    ./shells.nix
    ./tmux.nix
    ./fzf.nix
    ./gpg.nix
  ];

  kyle = {
    git.enable = true;
    tailscale.enable = true;
    neovim.enable = true;
    starship.enable = true;
  };

  boot.supportedFilesystems = [ "ntfs" ];

  environment = {
    variables = {
      ALTERNATE_EDITOR = "";
    };
  };

  time = {
    timeZone = "America/New_York";
    hardwareClockInLocalTime = true;
  };

  networking = {
    nameservers = [
      "1.1.1.1"
      "1.0.0.1"
    ];
    usePredictableInterfaceNames = false;
    firewall = {
      enable = true;
      allowPing = false;
    };
    wireguard.enable = true;
  };

  environment.systemPackages = with pkgs; [
    killall
    libcdio
    sg3_utils
    xfce.xfburn
  ];

  programs.fish.enable = true;
  users.users = {
    root = {
      initialPassword = "root";
    };
    kyle = {
      description = "Kyle Sferrazza";
      isNormalUser = true;
      group = "kyle";
      extraGroups = [ "wheel" ];
      initialPassword = "kyle";
      shell = pkgs.fish;
    };
  };
  users.groups.kyle = { };

  nix = {
    settings = {
      trusted-users = [ "@wheel" ];
    };
    # package = pkgs.nix;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  system.stateVersion = "24.05";
}
