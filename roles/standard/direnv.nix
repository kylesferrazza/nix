{ pkgs, config, ... }:
{
  home-manager.users.${config.kyle.username} = {
    programs.direnv = {
      enable = true;
      enableBashIntegration = true;
      # enableFishIntegration = true;
    };

    programs.git.ignores = [
      ".envrc"
      ".direnv"
    ];
  };
}
