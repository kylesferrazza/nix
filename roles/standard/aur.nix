{ pkgs, ... }:
{
  environment.systemPackages = [
    (pkgs.writeShellScriptBin "aur" ''
      docker pull registry.gitlab.com/kylesferrazza/docker-aur:latest
      ${pkgs.xorg.xhost}/bin/xhost +local:root
      docker run \
        --env="DISPLAY" \
        --env="QT_X11_NO_MITSHM=1" \
        --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
        -v $PWD:/app \
        --rm \
        -it registry.gitlab.com/kylesferrazza/docker-aur:latest
      ${pkgs.xorg.xhost}/bin/xhost -local:root
    '')
  ];
}
