{ pkgs, ... }:
let
  enter-fhs = pkgs.writeShellScriptBin "fhs" ''
    set -euo pipefail

    if [[ $# -eq 0 ]]; then
      nix-shell "${./shell.nix}"
    else
      nix-shell "${./shell.nix}" --argstr run "$*"
    fi
  '';
in
{
  environment.systemPackages = [ enter-fhs ];
}
