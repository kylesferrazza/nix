{
  pkgs ? import <nixpkgs> { },
  run ? "${pkgs.fish}/bin/fish",
}:
(pkgs.buildFHSUserEnv {
  name = "enter-fhs";
  targetPkgs =
    pkgs: with pkgs; [
      alsaLib
      atk
      cairo
      cups
      dbus
      expat
      file
      fontconfig
      freetype
      gdb
      git
      glib
      gnome2.GConf
      gdk-pixbuf
      gnome2.gtk
      gnome2.pango
      libnotify
      libxml2
      libxslt
      netcat
      nspr
      nss
      strace
      udev
      watch
      wget
      which
      xorg.libX11
      xorg.libXScrnSaver
      xorg.libXcomposite
      xorg.libXcursor
      xorg.libXdamage
      xorg.libXext
      xorg.libXfixes
      xorg.libXi
      xorg.libXrandr
      xorg.libXrender
      xorg.libXtst
      xorg.libxcb
      xorg.xcbutilkeysyms
      zlib
      zsh
    ];
  runScript = run;
}).env
