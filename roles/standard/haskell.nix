{ config, ... }:
{
  home-manager.users.${config.kyle.username} = {
    home.file.".ghci" = {
      text = ''
        :def hoogle \x -> return $ ":!hoogle \"" ++ x ++ "\""
        :def doc \x -> return $ ":!hoogle --info \"" ++ x ++ "\""
      '';
    };
    home.file.".stack/config.yaml" = {
      text = ''
        templates:
          params:
            author-name: Kyle Sferrazza
            author-email: kyle.sferrazza@gmail.com
            copyright: 2019 Kyle Sferrazza
            github-username: kylesferrazza
      '';
    };
  };
}
