{ config, pkgs, ... }:
{
  services.logind = {
    lidSwitch = "lock";
    extraConfig = ''
      HandlePowerKey=ignore
    '';
  };

  services.tlp = {
    enable = true;
    settings = {
      CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
      CPU_SCALING_GOVERNOR_ON_AC = "performance";
    };
  };

  boot.extraModulePackages = with config.boot.kernelPackages; [ acpi_call ];

  systemd.services.battery_check = {
    description = "Send notification if battery is low";
    serviceConfig = {
      Type = "oneshot";
      User = "kyle";
      ExecStart = pkgs.writeShellScript "battery_check" ''
        . <(udevadm info -q property -p /sys/class/power_supply/BAT0 |
            grep -E 'POWER_SUPPLY_(CAPACITY|STATUS)=')
        if [[ $POWER_SUPPLY_STATUS = Discharging && $POWER_SUPPLY_CAPACITY -lt 20 ]]; then
          ${pkgs.libnotify}/bin/notify-send -u critical "Low battery!" "$POWER_SUPPLY_CAPACITY%";
        fi
      '';
    };
    environment = {
      DISPLAY = ":0";
    };
    after = [ "display-manager.service" ];
    startAt = "*:00/5";
  };
}
