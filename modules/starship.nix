{ lib, config, ... }:
with lib;
with lib.types;
let
  cfg = config.kyle.starship;
in
{
  options.kyle.starship.enable = mkEnableOption "starship";
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username}.programs.starship = {
      enable = true;
      settings = {
        aws.disabled = true;
        gcloud.disabled = true;
        nix_shell.symbol = "  ";
      };
    };
  };
}
