{ lib, config, ... }:
with lib;
with lib.types;
let
  cfg = config.kyle.colors.solarized-light;
in
{
  options.kyle.colors.solarized-light = {
    enable = mkEnableOption "solarized-light";
  };
  config.kyle.colors = mkIf cfg.enable {
    cursor = "#000000";
    foreground = "#93a1a1";
    background = "#fdf6e3";
    black = {
      dark = "#002b36";
      light = "#657b83";
    };
    red = {
      dark = "#dc322f";
      light = "#dc322f";
    };
    green = {
      dark = "#859900";
      light = "#859900";
    };
    yellow = {
      dark = "#b58900";
      light = "#b58900";
    };
    blue = {
      dark = "#268bd2";
      light = "#268bd2";
    };
    magenta = {
      dark = "#6c71c4";
      light = "#6c71c4";
    };
    cyan = {
      dark = "#2aa198";
      light = "#2aa198";
    };
    white = {
      dark = "#93a1a1";
      light = "#fdf6e3";
    };
  };
}
