{ lib, ... }:
with lib;
with lib.types;
let
  darkLightType = {
    dark = mkOption { type = str; };
    light = mkOption { type = str; };
  };
in
{
  imports = [
    ./solarized-dark.nix
    ./solarized-light.nix
  ];
  options = {
    kyle.colors = {
      cursor = mkOption { type = str; };
      foreground = mkOption { type = str; };
      background = mkOption { type = str; };
      black = darkLightType;
      red = darkLightType;
      green = darkLightType;
      yellow = darkLightType;
      blue = darkLightType;
      magenta = darkLightType;
      cyan = darkLightType;
      white = darkLightType;
    };
  };
}
