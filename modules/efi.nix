{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.kyle.efi;
in
{
  options.kyle.efi = {
    enable = mkEnableOption "EFI";
  };
  config = mkIf cfg.enable {
    boot.loader.efi.canTouchEfiVariables = true;
    boot.loader.systemd-boot.enable = true;
  };
}
