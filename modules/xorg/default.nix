{
  pkgs,
  config,
  lib,
  util,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.xorg;
in
{
  options.kyle.xorg = {
    enable = mkEnableOption "xorg";
  };
  config = mkIf cfg.enable {

    home-manager.users.${config.kyle.username} = {
      xsession.enable = true;
      xresources.extraConfig = builtins.readFile ./Xresources;
      home.file.".XCompose".source = ./.XCompose;
    };

    hardware.pulseaudio.enable = lib.mkDefault true;
    users.users.kyle.extraGroups = [ "audio" ];

    services = {
      unclutter-xfixes.enable = true;
      libinput.enable = true;
      xserver = {
        enable = true;
        xkb = {
          variant = "basic";
          options = "compose:prsc,caps:backspace";
          layout = "us,us";
        };
        desktopManager.xterm.enable = true;
        displayManager.lightdm = {
          enable = true;
          background = util.bg;
          greeters.gtk = {
            enable = true;
            iconTheme = config.kyle.gtk.icons;
            theme = config.kyle.gtk.theme;
          };
        };
      };
    };
  };
}
