{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.i3;
  colorclip = pkgs.callPackage ./colorclip.nix { };
  screenclip = pkgs.callPackage ./screenclip.nix { };
  lock = "loginctl lock-session";
  xrandrGui = "exec ${pkgs.arandr}/bin/arandr";
in
{
  imports = [
    ./displays.nix
    ./picom.nix
    ./lock.nix
    ./light.nix
  ];

  options.kyle.i3 = {
    enable = mkEnableOption "i3";
  };

  config = mkIf cfg.enable {
    kyle = {
      xorg.enable = true;
      polybar.enable = true;
      dunst.enable = true;
      gammastep.enable = true;
      rofi.enable = true;
    };

    home-manager.users.${config.kyle.username} = {
      services.udiskie.enable = true;

      home.sessionVariables._JAVA_AWT_WM_NONREPARENTING = "1";

      xsession.windowManager.i3 = {
        enable = true;
        package = pkgs.i3-gaps;
        config = rec {
          modifier = "Mod4";
          bars = [ ];
          focus = {
            followMouse = false;
          };
          window.titlebar = false;
          gaps = {
            outer = 0;
            inner = 20;
            smartGaps = true;
          };
          startup = [
            {
              command = "systemctl --user restart polybar";
              always = true;
              notification = false;
            }
            {
              command = "setxkbmap -option \"caps:backspace\"";
              always = true;
              notification = false;
            }
          ];
          keybindings = {
            "${modifier}+1" = "workspace 1";
            "${modifier}+2" = "workspace 2";
            "${modifier}+3" = "workspace 3";
            "${modifier}+4" = "workspace 4";
            "${modifier}+5" = "workspace 5";
            "${modifier}+6" = "workspace 6";
            "${modifier}+7" = "workspace 7";
            "${modifier}+8" = "workspace 8";
            "${modifier}+9" = "workspace 9";
            "${modifier}+0" = "workspace 10";
            "XF86MonBrightnessUp" = "exec ${pkgs.light}/bin/light -A 5";
            "XF86MonBrightnessDown" = "exec ${pkgs.light}/bin/light -U 5";

            "${modifier}+Shift+1" = "move container to workspace 1";
            "${modifier}+Shift+2" = "move container to workspace 2";
            "${modifier}+Shift+3" = "move container to workspace 3";
            "${modifier}+Shift+4" = "move container to workspace 4";
            "${modifier}+Shift+5" = "move container to workspace 5";
            "${modifier}+Shift+6" = "move container to workspace 6";
            "${modifier}+Shift+7" = "move container to workspace 7";
            "${modifier}+Shift+8" = "move container to workspace 8";
            "${modifier}+Shift+9" = "move container to workspace 9";
            "${modifier}+Shift+0" = "move container to workspace 10";

            "${modifier}+h" = "focus left";
            "${modifier}+j" = "focus down";
            "${modifier}+k" = "focus up";
            "${modifier}+l" = "focus right";

            "${modifier}+Shift+h" = "move left";
            "${modifier}+Shift+j" = "move down";
            "${modifier}+Shift+k" = "move up";
            "${modifier}+Shift+l" = "move right";

            "${modifier}+i" = "split h";
            "${modifier}+o" = "split v";
            "${modifier}+s" = "layout toggle split";
            "${modifier}+f" = "fullscreen";
            "${modifier}+Tab" = "layout toggle tabbed splith";

            "${modifier}+Shift+space" = "floating toggle";
            "${modifier}+space" = "focus mode_toggle";

            "${modifier}+Shift+c" = "reload";
            "${modifier}+Shift+r" = "restart";
            "${modifier}+Shift+e" = "exit";

            "${modifier}+r" = "mode resize";

            "${modifier}+Return" = "exec $TERMINAL";

            "${modifier}+d" = "exec rofi -show drun";
            "${modifier}+Shift+d" = "exec rofi -show run";
            "${modifier}+w" = "exec rofi -show window";

            "${modifier}+Escape" = "exec poweroff";
            "${modifier}+Shift+Escape" = "exec reboot";
            "${modifier}+backslash" = "exec ${config.kyle.browser.cmd}";
            "${modifier}+BackSpace" = "exec ${pkgs.xfce.thunar-bare}/bin/thunar";
            "${modifier}+a" = "exec ${pkgs.pavucontrol}/bin/pavucontrol";
            "${modifier}+t" = "exec ${pkgs.mine.termlaunch}/bin/termlaunch '${pkgs.htop}/bin/htop'";
            "${modifier}+Shift+t" =
              "exec ${pkgs.mine.termlaunch}/bin/termlaunch 'sudo ${pkgs.powertop}/bin/powertop'";
            "${modifier}+n" = "exec ${pkgs.networkmanagerapplet}/bin/nm-connection-editor";
            "${modifier}+q" = "kill";
            "${modifier}+grave" = "exec ${lock}";
            "XF86Sleep" = "exec ${lock}";
            "XF86PowerOff" = "exec ${lock}";
            "${modifier}+equal" = "exec ${screenclip}/bin/screenclip";
            "${modifier}+Shift+equal" = "exec ${screenclip}/bin/screenclip -file";
            "${modifier}+minus" = "exec ${colorclip}/bin/colorclip";
            "XF86Bluetooth" = "exec ${pkgs.xorg.xkill}/bin/xkill";

            "XF86AudioRaiseVolume" = "exec ${pkgs.alsaUtils}/bin/amixer set Master 10%+";
            "XF86AudioLowerVolume" = "exec ${pkgs.alsaUtils}/bin/amixer set Master 10%-";
            "XF86AudioMute" =
              "exec ${pkgs.alsaUtils}/bin/amixer set Master 0% && exec ${pkgs.alsaUtils}/bin/amixer set Master toggle";

            "${modifier}+p" = xrandrGui;
            "XF86Display" = xrandrGui;

            # MACROS
            "${modifier}+F1" =
              "exec sleep 0.2 && exec ${pkgs.xdotool}/bin/xdotool type 'kyle.sferrazza@gmail.com'";
            "${modifier}+F2" = "exec sleep 0.2 && exec ${pkgs.xdotool}/bin/xdotool type 'Kyle Sferrazza'";
            "${modifier}+F3" =
              "exec sleep 0.2 && exec ${pkgs.xdotool}/bin/xdotool type \"$(date +'%-d %B %Y')\"";

            "${modifier}+e" = "exec ${pkgs.mine.evim}/bin/evim";

            # MUSIC
            "XF86AudioPlay" = "exec ${pkgs.playerctl}/bin/playerctl play-pause";
            "${modifier}+Shift+slash" = "exec ${pkgs.playerctl}/bin/playerctl play-pause";

            "XF86AudioPrev" = "exec ${pkgs.playerctl}/bin/playerctl previous";
            "${modifier}+Shift+comma" = "exec ${pkgs.playerctl}/bin/playerctl previous";

            "XF86AudioNext" = "exec ${pkgs.playerctl}/bin/playerctl next";
            "${modifier}+Shift+period" = "exec ${pkgs.playerctl}/bin/playerctl next";
          };
          modes = {
            resize = {
              H = "resize grow width 10 px or 10 ppt";
              J = "resize grow height 10 px or 10 ppt";
              K = "resize shrink height 10 px or 10 ppt";
              L = "resize shrink width 10 px or 10 ppt";
              Return = "mode default";
              Escape = "mode default";
            };
          };
        };

        extraConfig = ''
          set $base00 #002b36
          set $base01 #073642
          set $base02 #586e75
          set $base03 #657b83
          set $base04 #839496
          set $base05 #93a1a1
          set $base06 #eee8d5
          set $base07 #fdf6e3
          set $base08 #dc322f
          set $base09 #cb4b16
          set $base0A #b58900
          set $base0B #859900
          set $base0C #2aa198
          set $base0D #268bd2
          set $base0E #6c71c4
          set $base0F #d33682

          client.focused          $base05 $base0D $base00 $base0D $base0C
          client.focused_inactive $base01 $base01 $base05 $base03 $base01
          client.unfocused        $base01 $base00 $base05 $base01 $base01
          client.urgent           $base08 $base08 $base00 $base08 $base08
          client.placeholder      $base00 $base00 $base05 $base00 $base00
          client.background       $base07

          workspace_auto_back_and_forth yes

          default_border pixel 2
          focus_follows_mouse no

          new_window pixel 2

          popup_during_fullscreen leave_fullscreen

          for_window [class="Pinentry"] floating enable
          for_window [title="(?i)Starting Unity"] floating enable
          for_window [title="^World$"] floating enable
          for_window [title="^win0$"] floating enable
          for_window [class="mpv"] floating enable
          for_window [class="feh"] floating enable

          exec --no-startup-id i3-msg workspace 1
        '';
      };
    };
  };
}
