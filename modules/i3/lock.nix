{
  pkgs,
  util,
  lib,
  config,
  ...
}:
let
  updatelock = pkgs.writeShellScriptBin "updatelock" ''
    ${pkgs.multilockscreen}/bin/multilockscreen -u ${util.bg} --fx ""
  '';
in
{
  config = lib.mkIf config.kyle.i3.enable {
    home-manager.users.${config.kyle.username} = {
      home.packages = [ updatelock ];
      xdg.configFile."multilock/config".text = ''
        font="Iosevka Semibold"
      '';
      services.screen-locker = {
        enable = true;
        inactiveInterval = 10;
        lockCmd = "${pkgs.multilockscreen}/bin/multilockscreen -l";
        xautolock.extraOptions = [
          "-corners 0+0-"
          "-cornerdelay 1"
          "-cornerredelay 10"
        ];
      };
    };
  };
}
