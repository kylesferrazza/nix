{
  writeShellScriptBin,
  bash,
  xdotool,
  imagemagick,
  gnugrep,
  xclip,
  libnotify,
}:
writeShellScriptBin "colorclip" ''
  eval $(${xdotool}/bin/xdotool getmouselocation --shell)
  IMAGE=`${imagemagick}/bin/import -window root -depth 8 -crop 1x1+$X+$Y txt:-`
  COLOR=`echo $IMAGE | ${gnugrep}/bin/grep -om1 '#\w\+'`
  # if ! ${gnugrep}/bin/grep "#" <(echo $COLOR) > /dev/null; then
  #   exit 1 # not a color
  # fi
  IMG=$(mktemp --suffix ".png")
  ${imagemagick}/bin/convert -size 50x50 "xc:$COLOR" "$IMG"
  echo -n $COLOR | ${xclip}/bin/xclip -i -selection CLIPBOARD
  ${libnotify}/bin/notify-send -i "$IMG" "$COLOR copied to clipboard."
  rm "$IMG"
''
