{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.i3.dualMonitor;
  monitorOptions = {
    name = mkOption { type = str; };
    resolution = mkOption { type = str; };
    refresh = mkOption { type = str; };
  };
in
{
  options.kyle.i3.dualMonitor = {
    enable = mkEnableOption "dualMonitor";
    left = monitorOptions;
    right = monitorOptions;
  };

  config = mkIf cfg.enable {
    services.xserver.displayManager.setupCommands = ''
      ${pkgs.xorg.xrandr}/bin/xrandr --output ${cfg.left.name} --mode ${cfg.left.resolution} --refresh ${cfg.left.refresh} --primary --left-of ${cfg.right.name} --mode ${cfg.right.resolution} --refresh ${cfg.right.refresh}
    '';
    home-manager.users.${config.kyle.username}.xsession.windowManager.i3.extraConfig = ''
      workspace 1 output ${cfg.left.name}
    '';
  };
}
