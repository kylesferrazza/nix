{
  writeScriptBin,
  coreutils,
  maim,
  xclip,
  utillinux,
  libnotify,
}:
writeScriptBin "screenclip" ''
  TMP=$(${coreutils}/bin/mktemp --suffix=.png)
  ${maim}/bin/maim -u -s $TMP
  cat $TMP | ${xclip}/bin/xclip -selection clipboard -t image/png
  if [ "$1" = "-file" ]; then
    UUIDGEN="$(${utillinux}/bin/uuidgen -t)"
    SAVEPATH="$HOME/Downloads/$UUIDGEN.png"
    echo "$SAVEPATH" | ${xclip}/bin/xclip -selection clipboard
    ${libnotify}/bin/notify-send -i "$TMP" "Screenshot path copied to clipboard."
    mv "$TMP" "$SAVEPATH"
  else
    ${libnotify}/bin/notify-send -i "$TMP" "Screenshot copied to clipboard."
    rm -f $TMP
  fi
''
