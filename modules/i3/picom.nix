{ lib, config, ... }:
{
  config = lib.mkIf config.kyle.i3.enable {
    home-manager.users.${config.kyle.username}.services.picom = {
      backend = "glx";
      enable = true;
      shadow = true;
      shadowExclude = [
        "class_g = 'Polybar'"
        "class_g = 'slop'"
      ];
      fade = true;
      fadeDelta = 2;
      settings = {
        xinerama-shadow-crop = true;
        shadow-exclude-reg = "x30+0+0";
      };
      vSync = true;
    };
  };
}
