{
  pkgs,
  config,
  lib,
  ...
}:
{
  config = lib.mkIf config.kyle.i3.enable {
    home-manager.users.${config.kyle.username}.xsession.windowManager.i3.config.keybindings = {
      "XF86MonBrightnessUp" = "exec ${pkgs.light}/bin/light -A 5";
      "XF86MonBrightnessDown" = "exec ${pkgs.light}/bin/light -U 5";
    };
  };
}
