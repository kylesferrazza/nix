{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.virtualisation;
in
{
  options.kyle.virtualisation = {
    enable = mkEnableOption "virtualisation";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      virt-manager
      nixos-generators
    ];
    virtualisation.virtualbox.host.enable = true;
    users.users.kyle.extraGroups = [
      "libvirtd"
      "kvm"
      "input"
    ];
    virtualisation.libvirtd = {
      enable = true;
      qemu = {
        ovmf.enable = true;
        runAsRoot = true;
        verbatimConfig = ''
          user = "kyle"
          cgroup_device_acl = [
            "/dev/kvm",
            "/dev/input/event0",
            "/dev/input/event2",
            "/dev/null", "/dev/full", "/dev/zero",
            "/dev/random", "/dev/urandom",
            "/dev/ptmx", "/dev/kvm", "/dev/kqemu",
            "/dev/rtc","/dev/hpet", "/dev/sev"
          ]
        '';
      };
      onBoot = "ignore";
      onShutdown = "shutdown";
    };
  };
}
