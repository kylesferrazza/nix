{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.kyle.pxe;
  tftpdir = "tftpboot";
  network = "10.99.0.0";
  netbootVersion = "2.0.29";
in
{
  options.kyle.pxe = {
    enable = mkEnableOption "pxe";
  };
  config = mkIf cfg.enable {
    networking.firewall.allowedUDPPorts = [
      67 # dhcp/bootp
      69 # tftp
      4011 # pxe
    ];
    services.dnsmasq = {
      enable = true;
      resolveLocalQueries = false;
      extraConfig = ''
        # Disable DNS Server
        port=0

        # Enable DHCP logging
        log-dhcp

        # Respond to PXE requests for the specified network;
        # run as DHCP proxy
        dhcp-range=${network},proxy

        # set tag "ENH" if request comes from iPXE ("iPXE" user class)
        dhcp-userclass=set:ENH,iPXE

        # Provide network boot option called "Network Boot".
        pxe-service=tag:!ENH,x86PC,"iPXE",undionly.kpxe
        pxe-service=tag:!ENH,X86-64_EFI,"iPXE",ipxe.efi

        enable-tftp
        tftp-root=/etc/${tftpdir}
      '';
    };

    environment.etc."${tftpdir}/ipxe.efi".source = builtins.fetchurl {
      url = "https://github.com/netbootxyz/netboot.xyz/releases/download/${netbootVersion}/netboot.xyz.efi";
      sha256 = "08mb9awg9a81mryvkh2fc5xvxgz3j6h2i0w2yf2a0pyaqz0kj07p";
    };

    environment.etc."${tftpdir}/undionly.kpxe".source = builtins.fetchurl {
      url = "https://github.com/netbootxyz/netboot.xyz/releases/download/${netbootVersion}/netboot.xyz-undionly.kpxe";
      sha256 = "0g53r2gxz2nfjd1kvwi2mr5kk3977nkqps1vd5k22ka46pwvskp1";
    };
  };
}
