{
  pkgs,
  config,
  lib,
  util,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.wayland;
in
{
  options.kyle.wayland = {
    enable = mkEnableOption "wayland";
  };
  config = mkIf cfg.enable {
    # Sound
    security.rtkit.enable = true;

    hardware.pulseaudio.enable = lib.mkForce false;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      # If you want to use JACK applications, uncomment this
      #jack.enable = true;
    };

    xdg.portal = {
      enable = true;
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };

    environment.variables = {
      NIXOS_OZONE_WL = "1";
    };

    environment.systemPackages = with pkgs; [ libnotify ];
    home-manager.users.${config.kyle.username}.services.mako = {
      enable = true;
      backgroundColor = config.kyle.colors.background;
      borderColor = config.kyle.colors.cyan.dark;
    };
  };
}
