{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.browser.firefox;
in
{
  options.kyle.browser.firefox = {
    enable = mkEnableOption "firefox";
  };
  config = mkIf cfg.enable {
    kyle.browser.cmd = "firefox-esr";
    home-manager.users.${config.kyle.username}.programs.firefox = {
      enable = true;
      package = pkgs.firefox-esr;
      policies = {
        DisablePocket = true;
        OfferToSaveLogins = false;
        DisableFirefoxStudies = true;
        DisableTelemetry = true;
        PasswordManagerEnabled = false;
        OverrideFirstRunPage = "about:blank";
        OverridePostUpdatePage = "about:blank";
        SearchEngines = {
          Add = [
            {
              Name = "Kagi";
              URLTemplate = "https://kagi.com/search?q={searchTerms}";
            }
          ];
          Default = "Kagi";
        };
        Homepage.StartPage = "previous-session";
        DisplayBookmarksToolbar = false;
        ExtensionSettings = {
          "*" = {
            default_area = "navbar";
          };
        };
      };
      profiles.default = {
        # https://github.com/nix-community/nur-combined/blob/master/repos/rycee/pkgs/firefox-addons/generated-firefox-addons.nix
        extensions = with config.nur.repos.rycee.firefox-addons; [
          onepassword-password-manager
          vimium
          sidebery
          multi-account-containers
          ublock-origin
          solarized_fox
        ];
        settings = {
          "extensions.autoDisableScopes" = 0;
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
          "extensions.pocket.enabled" = false;
          "browser.urlbar.suggest.quicksuggest.sponsored" = false;
          "browser.urlbar.sponsoredTopSites" = false;
          "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
          "browser.newtabpage.activity-stream.showSponsored" = false;
          "browser.newtabpage.activity-stream.section.highlights.includePocket" = false;
          "browser.newtabpage.activity-stream.feeds.section.topstories" = false;
          "extensions.pocket.api" = "localhost";
        };
        userChrome = builtins.readFile ./userChrome.css;
      };
    };
  };
}
