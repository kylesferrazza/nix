{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.browser.vivaldi;
in
{
  options.kyle.browser.vivaldi = {
    enable = mkEnableOption "vivaldi";
  };
  config = mkIf cfg.enable {
    kyle.browser.cmd = "vivaldi";
    nixpkgs.overlays = [
      (self: super: {
        vivaldi = super.vivaldi.override {
          commandLineArgs = lib.concatStringsSep " " [
            # Waiting for:
            # https://gitlab.gnome.org/GNOME/Initiatives/-/wikis/Dark-Style-Preference
            "--enable-features=WebUIDarkMode"
            "--force-dark-mode"
          ];
        };
      })
    ];
    environment.systemPackages = [ pkgs.vivaldi ];
    environment.etc."1password/custom_allowed_browsers" = {
      text = ''
        vivaldi
        vivaldi-bin
      '';
      mode = "0755";
    };
  };
}
