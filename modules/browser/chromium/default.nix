{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.browser.chromium;
in
{
  options.kyle.browser.chromium = {
    enable = mkEnableOption "chromium";
  };
  config = mkIf cfg.enable {
    kyle.browser.cmd = "chromium";
    nixpkgs.overlays = [
      (self: super: {
        chromium = super.chromium.override {
          commandLineArgs = lib.concatStringsSep " " [
            # Waiting for:
            # https://gitlab.gnome.org/GNOME/Initiatives/-/wikis/Dark-Style-Preference
            "--enable-features=WebUIDarkMode"
            "--force-dark-mode"
          ];
        };
      })
    ];
    environment.systemPackages = [ pkgs.chromium ];
    programs.chromium = {
      enable = true;
      extensions = [
        "aeblfdkhhhdcdjpifhhbdiojplfjncoa" # 1password
        "dbepggeogbaibhgnhhndojpepiihcmeb" # vimium
        "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock origin
      ];
      extraOpts = {
        "BrowserThemeColor" = config.kyle.colors.background;
        "PasswordManagerEnabled" = false;
        "DefaultBrowserSettingEnabled" = true;
      };
    };
  };
}
