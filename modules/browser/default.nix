{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types;
{
  imports = [
    ./firefox
    ./chromium
    ./vivaldi
  ];
  options.kyle.browser.cmd = mkOption { type = str; };
  config = {
    home-manager.users.${config.kyle.username}.home.packages = [
      (pkgs.writeShellScriptBin "x-www-browser" ''
        ${config.kyle.browser.cmd} $@
      '')
    ];
  };
}
