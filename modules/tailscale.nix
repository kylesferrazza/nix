{ lib, config, ... }:
with lib;
with lib.types;
let
  cfg = config.kyle.tailscale;
in
{
  options.kyle.tailscale.enable = mkEnableOption "tailscale";
  config = mkIf cfg.enable {
    services.tailscale = {
      enable = true;
      useRoutingFeatures = "both";
      extraUpFlags = [
        "--ssh"
        "--advertise-exit-node"
        "--advertise-routes=10.99.0.0/16"
      ];
    };
  };
}
