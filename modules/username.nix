{ lib, ... }:
with lib;
with lib.types;
{
  options.kyle.username = mkOption { type = str; };
  config.kyle.username = lib.mkDefault "kyle";
}
