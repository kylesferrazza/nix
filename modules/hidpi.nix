{ lib, config, ... }:
with lib;
let
  cfg = config.kyle.hidpi;
in
{
  options.kyle.hidpi = {
    enable = mkEnableOption "High DPI";
  };
  config = mkIf cfg.enable {
    services.xserver.dpi = 130;
    home-manager.users.${config.kyle.username}.xresources.extraConfig = ''
      Xft.dpi: 130
    '';
  };
}
