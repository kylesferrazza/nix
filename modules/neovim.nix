{
  extras,
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.kyle.neovim;
in
{
  options.kyle.neovim.enable = mkEnableOption "neovim";
  options.kyle.neovim.default = mkOption {
    type = types.bool;
    default = false;
  };
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username} = {
      home.packages = [ extras.neovim-config.defaultPackage.x86_64-linux ];
      home.sessionVariables.EDITOR = mkIf cfg.default "nvim";
      xdg.configFile."nvim/coc-settings.json".text = ''
        {
          "rust-client.disableRustup": true,
          "solargraph.diagnostics": true,
          "solargraph.autoformat": true,
          "solargraph.formatting": true,
          "solargraph.hover": true,
          "typescript.format.insertSpaceAfterOpeningAndBeforeClosingNonemptyBraces": true,
          "languageserver": {
            "clojure-lsp": {
              "command": "bash",
              "args": ["-c", "clojure-lsp"],
              "filetypes": ["clojure"],
              "rootPatterns": ["project.clj"],
              "additionalSchemes": ["jar", "zipfile"],
              "trace.server": "verbose",
              "initializationOptions": {
              }
            },
            "ccls": {
              "command": "ccls",
              "filetypes": ["c", "cpp", "cuda", "objc", "objcpp"],
              "rootPatterns": [".ccls-root", "compile_commands.json"],
              "initializationOptions": {
                "cache": {
                  "directory": ".ccls-cache"
                }
              }
            },
            "golang": {
              "command": "gopls",
              "rootPatterns": ["go.mod"],
              "filetypes": ["go"],
              "initializationOptions": {
                "usePlaceholders": true
              }
            }
          }
        }
      '';
    };
  };
}
