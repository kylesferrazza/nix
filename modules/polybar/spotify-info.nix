{
  writeShellScriptBin,
  bash,
  playerctl,
}:
writeShellScriptBin "spotify-info" ''
  STAT="$(${playerctl}/bin/playerctl status)"
  if test "$STAT" = "Playing" -o "$STAT" = "Paused"; then
    echo "$(${playerctl}/bin/playerctl metadata title) - $(${playerctl}/bin/playerctl metadata artist)"
  else
    exit 1
  fi
''
