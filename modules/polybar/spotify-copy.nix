{
  writeShellScriptBin,
  bash,
  coreutils,
  playerctl,
  wget,
  xclip,
  libnotify,
  spotify-info,
  gawk,
}:
writeShellScriptBin "spotify-copy" ''
  IMG=$(${coreutils}/bin/mktemp --suffix=.jpg)
  ART_ID=$(${playerctl}/bin/playerctl metadata mpris:artUrl | ${gawk}/bin/awk -F '/' '{ print $5 }')
  ART_URL="https://i.scdn.co/image/$ART_ID"
  ${wget}/bin/wget -O "$IMG" "$ART_URL"
  URL=$(${playerctl}/bin/playerctl metadata xesam:url | ${coreutils}/bin/tr -d '\n')
  echo -n $URL | ${xclip}/bin/xclip -selection c
  ${libnotify}/bin/notify-send -i "$IMG" "$(${spotify-info}/bin/spotify-info)" "Song URL copied to clipboard."
  rm "$IMG"
''
