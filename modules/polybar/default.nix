{
  util,
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.polybar;
  spotify-info = pkgs.callPackage ./spotify-info.nix { };
  spotify-copy = pkgs.callPackage ./spotify-copy.nix { inherit spotify-info; };
  copy-ip = pkgs.callPackage ./copy-ip.nix { };
  vpn-address = pkgs.writeShellScriptBin "vpn-address" ''
    ${pkgs.nettools}/bin/ifconfig tun0 | ${pkgs.gawk}/bin/awk '{print $2}' | ${pkgs.coreutils}/bin/head -n 2 | ${pkgs.coreutils}/bin/tail -n 1
  '';
  vpn-active = pkgs.writeShellScriptBin "vpn-active" ''
    ${pkgs.coreutils}/bin/ls /sys/class/net/tun0
  '';
in
{
  options.kyle.polybar = {
    enable = mkEnableOption "polybar";
  };
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username}.services.polybar = rec {
      enable = true;
      package = pkgs.polybar.override {
        i3Support = true;
        pulseSupport = true;
      };

      script =
        let
          binPath = pkgs.lib.makeBinPath [
            pkgs.xorg.xrandr
            pkgs.killall
            pkgs.coreutils
            pkgs.gnugrep
            package
          ];
        in
        ''
          #!/usr/bin/env bash
          # from github.com/woefe/dotfiles

          PATH=${binPath}:PATH

          # Launches the `mirrored` configuration if two monitors are connected which have the same resolution and position.
          # Otherwise, launches the `primary` bar on the primary monitor and the `secondary` bar on all secondary monitors.

          readarray -t mirrors < <(xrandr --query | grep " connected" | grep -Eo '[0-9]+x[0-9]+\+[0-9]+\+[0-9]+')
          echo "MIRRORS: $mirrors"
          if (( ''${#mirrors[@]} == 2 )) && [[ "''${mirrors[0]}" != "" && "''${mirrors[0]}" == "''${mirrors[1]}" ]]; then
            echo "STARTING MIRRORED"
            polybar mirrored &
            exit 0
          fi
          echo "NOT MIRRORED."

          primary_monitor=$(xrandr --query | grep " connected primary" | cut -d" "  -f1)
          secondary_monitors=$(xrandr --query | grep " connected" | grep -v " primary" | cut -d" "  -f1)

          if [ -n "''${primary_monitor}" ]; then
            echo "STARTING PRIMARY"
            MONITOR=''${primary_monitor} polybar primary &
          fi

          for m in $secondary_monitors; do
            echo "STARTING SECONDARY"
            MONITOR=$m polybar secondary &
          done
        '';

      extraConfig =
        let
          colors = {
            background = "#002b36";
            background-alt = "#073642";
            foreground = "#bbb";
            foreground-alt = "#bbb";
            primary = "#018e92";
            secondary = "#2d7385";
            alert = "#bd2c40";
          };
        in
        ''
          [bar/primary]
          bottom = false
          monitor = ''${env:MONITOR}
          width = 100%
          height = 30
          fixed-center = false


          background = ${colors.background}
          foreground = ${colors.foreground}

          line-size = 3
          line-color = #f00

          border-size = 0
          border-color = #00000000

          padding-left = 0
          padding-right = 1

          module-margin-left = 0
          module-margin-right = 3

          font-0 = "Iosevka:pixelsize=10;1"
          font-1 = "Font Awesome ${util.fontAwesomeVersion} Free:style=Solid;2"

          modules-left = workspaces
          modules-center =
          modules-right = xkeyboard music backlight wlan ether vpn volume battery temperature date time

          tray-position = right
          tray-padding = 3
          tray-maxsize = 28
          tray-background = ${colors.background}

          [bar/secondary]
          inherit = bar/primary
          ; modules-left = workspacesmirrored

          [bar/mirrored]
          inherit = bar/primary
          monitor =
          modules-left = workspacesmirrored

          [module/music]
          type = custom/script
          format = "<label>"
          format-prefix = " "
          format-prefix-font = 2
          exec = ${spotify-info}/bin/spotify-info
          exec-if = ${spotify-info}/bin/spotify-info
          click-left = ${spotify-copy}/bin/spotify-copy
          interval = 10

          [module/backlight]
          type = internal/backlight
          card = intel_backlight
          format-prefix = " "
          format-prefix-font = 2

          [module/xkeyboard]
          type = internal/xkeyboard
          format-prefix = " "
          label-layout = %name%
          ; label-indicator-padding = 2
          ; label-indicator-margin = 1
          label-indicator-background = ${colors.secondary}

          [module/workspaces]
          type = internal/i3
          pin-workspaces = true
          format = <label-state> <label-mode>
          wrapping-scroll = false

          label-mode-padding = 2
          label-mode-foreground = #000
          label-mode-background = ${colors.primary}

          ; focused = Active workspace on focused monitor
          label-focused = %name%
          label-focused-background = ${colors.background-alt}
          label-focused-padding = 2

          ; unfocused = Inactive workspace on any monitor
          label-unfocused = %name%
          label-unfocused-padding = 2

          ; visible = Active workspace on unfocused monitor
          label-visible = %name%
          label-visible-background = ${colors.background-alt}
          label-visible-padding = 2

          ; urgent = Workspace with urgency hint set
          label-urgent = %name%
          label-urgent-background = ${colors.alert}
          label-urgent-padding = 2

          label-focused-underline = #2aa198

          [module/workspacesmirrored]
          inherit = module/workspaces
          pin-workspaces = false

          [module/wlan]
          type = internal/network
          interface = wlan0
          interval = 3.0

          format-connected = %{A:${copy-ip}/bin/copy-ip wlan0&:}<ramp-signal><label-connected>%{A}
          label-connected = %local_ip%

          format-disconnected =

          ramp-signal-0 = " "
          ramp-signal-font = 2
          ramp-signal-foreground = ${colors.foreground-alt}

          [module/ether]
          type = internal/network
          interface = eth0
          interval = 3.0

          format-connected = %{A:${copy-ip}/bin/copy-ip eth0&:}<ramp-signal><label-connected>%{A}
          label-connected = %local_ip%

          format-disconnected =

          ramp-signal-0 = " "
          ramp-signal-font = 2
          ramp-signal-foreground = ${colors.foreground-alt}

          [module/vpn]
          type = custom/script
          exec = ${vpn-address}/bin/vpn-address
          exec-if = ${vpn-active}/bin/vpn-active
          interval = 5

          format-prefix = " "
          format-prefix-font = 2
          format-prefix-foreground = ${colors.foreground-alt}
          click-left = ${copy-ip}/bin/copy-ip tun0

          [module/date]
          type = internal/date
          interval = 5

          date = "%D"

          format-prefix = " "
          format-prefix-font = 2
          format-prefix-foreground = ${colors.foreground-alt}

          label = "%date%"

          [module/time]
          type = internal/date
          interval = 5

          time = "%H:%M"

          format-prefix = " "
          format-prefix-font = 2
          format-prefix-foreground = ${colors.foreground-alt}

          label = "%time%"

          [module/volume]
          type = internal/pulseaudio

          format-volume = <ramp-volume><label-volume>
          label-volume = %percentage%%
          label-volume-foreground = ${colors.foreground}

          ramp-volume-0 = " "
          ramp-volume-1 = " "
          ramp-volume-2 = " "
          ramp-volume-font = 2

          format-muted = "muted"
          format-muted-foreground = ${colors.foreground-alt}
          format-muted-prefix = " "
          format-muted-prefix-font = 2

          [module/battery]
          type = internal/battery
          battery = BAT0
          adapter = AC
          full-at = 98

          format-charging = <label-charging>
          format-charging-prefix = " "
          format-charging-prefix-font = 2
          format-discharging = <ramp-capacity><label-discharging>
          format-full = <label-full>

          format-full-prefix = " "
          format-full-prefix-font = 2
          format-full-prefix-foreground = ${colors.foreground-alt}

          ramp-capacity-0 = " "
          ramp-capacity-1 = " "
          ramp-capacity-2 = " "
          ramp-capacity-3 = " "
          ramp-capacity-4 = " "
          ramp-capacity-font = 2
          ramp-capacity-foreground = ${colors.foreground-alt}

          [module/temperature]
          type = internal/temperature
          thermal-zone = 0
          warn-temperature = 60

          format = <ramp><label>
          format-warn = <ramp><label-warn>

          label = %temperature-c%
          label-warn = %temperature-c%
          label-warn-foreground = ${colors.secondary}

          ramp-0 = " "
          ramp-1 = " "
          ramp-2 = " "
          ramp-3 = " "
          ramp-4 = " "
          ramp-font = 2
          ramp-foreground = ${colors.foreground-alt}

          [settings]
          screenchange-reload = true

          [global/wm]
          margin-top = 5
          margin-bottom = 5
        '';
    };
  };
}
