{
  writeScriptBin,
  nettools,
  gawk,
  coreutils,
  libnotify,
  xclip,
}:
writeScriptBin "copy-ip" ''
  IP=$(${nettools}/bin/ifconfig $1 | ${gawk}/bin/awk '{print $2}' | ${coreutils}/bin/head -n 2 | ${coreutils}/bin/tail -n 1)
  echo -n "$IP" | ${xclip}/bin/xclip -selection c
  ${libnotify}/bin/notify-send "$IP" "IP copied to clipboard."
''
