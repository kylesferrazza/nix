{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.nvidia;
in
{
  options.kyle.nvidia = {
    enable = mkEnableOption "nvidia";
  };
  config = mkIf cfg.enable {
    services.xserver.videoDrivers = [ "nvidia" ];
    hardware.graphics = {
      enable = true;
      enable32Bit = true;
    };
    hardware.nvidia = {
      modesetting.enable = true;
      nvidiaSettings = true;
      open = false;
    };
    # hardware.nvidia.prime = {
    #   offload.enable = true;
    #   nvidiaBusId = "PCI:1:0:0";
    #   intelBusId = "PCI:7:0:0";
    # };
    environment.systemPackages = [
      (pkgs.writeShellScriptBin "nvidia-offload" ''
        export __NV_PRIME_RENDER_OFFLOAD=1
        export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
        export __GLX_VENDOR_LIBRARY_NAME=nvidia
        export __VK_LAYER_NV_optimus=NVIDIA_only
        exec -a "$0" "$@"
      '')
    ];
  };
}
