{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.gtk.solarized-dark;
in
{
  options.kyle.gtk.solarized-dark = {
    enable = mkEnableOption "solarized-dark";
  };
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username}.dconf.settings."org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
    kyle.gtk = {
      theme = {
        package = pkgs.numix-solarized-gtk-theme;
        name = "NumixSolarizedDarkCyan";
      };
      icons = {
        package = pkgs.paper-icon-theme;
        name = "Paper";
      };
    };
  };
}
