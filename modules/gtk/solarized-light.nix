{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.gtk.solarized-light;
in
{
  options.kyle.gtk.solarized-light = {
    enable = mkEnableOption "solarized-light";
  };
  config.kyle.gtk = mkIf cfg.enable {
    theme = {
      package = pkgs.numix-solarized-gtk-theme;
      name = "NumixSolarizedLightCyan";
    };
    icons = {
      package = pkgs.paper-icon-theme;
      name = "Paper";
    };
  };
}
