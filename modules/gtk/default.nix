{ lib, config, ... }:
with lib;
with lib.types;
let
  cfg = config.kyle.gtk;
  gtkThemeType = submodule {
    options = {
      package = mkOption { type = package; };
      name = mkOption { type = str; };
    };
  };
in
{
  imports = [
    ./solarized-dark.nix
    ./solarized-light.nix
  ];
  options = {
    kyle.gtk = {
      enable = mkEnableOption "gtk";
      icons = mkOption { type = gtkThemeType; };
      theme = mkOption { type = gtkThemeType; };
    };
  };
  config = mkIf cfg.enable {
    environment.sessionVariables.GTK_THEME = cfg.gtk.theme.name;
    programs.dconf.enable = true;
    home-manager.users.${config.kyle.username} = {
      dconf.settings."org/gnome/desktop/interface" = {
        gtk-theme = cfg.gtk.theme.name;
      };
      home = {
        pointerCursor = {
          gtk.enable = true;
          package = cfg.gtk.icons.package;
          name = cfg.gtk.icons.name;
          size = 16;
        };
      };
    };
  };
}
