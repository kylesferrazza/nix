{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.kyle.git;
in
{
  options.kyle.git = {
    enable = mkEnableOption "git";
  };
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username} = {
      programs.git = {
        enable = true;
        userName = "Kyle Sferrazza";
        userEmail = "git@kylesferrazza.com";
        signing = {
          key = "key::ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMKfa9ng8XLgftU/lzP25N7+ad19prxFKjNX99e/mJ+Y";
          signByDefault = true;
        };
        aliases = {
          a = "add --all .";
          s = "status";
          c = "commit -m";
          p = "push";
          d = "diff";
          ds = "diff --staged";
          dt = "difftool";
          dts = "difftool --staged";
          r = "reset --hard HEAD";
          m = "merge";
          i = "\"!f(){ git rm -r --cached . && git a; };f\"";
          mt = "mergetool";
          l = "log --graph --decorate --pretty=oneline --abbrev-commit --all";
          z = "archive HEAD -o";
        };
        ignores = [
          ".venv"
          ".idea"
          ".DS_Store"
        ];
        extraConfig = {
          gpg.format = "ssh";
          core = {
            pager = "${pkgs.diff-so-fancy}/bin/diff-so-fancy | less --tabs=4 -RFX";
          };
          credential = {
            helper = "cache";
          };
          color = {
            diff-highlight = {
              oldNormal = "red bold";
              oldHighlight = "red bold 52";
              newNormal = "green bold";
              newHighlight = "green bold 22";
            };
            diff = {
              meta = "yellow";
              frag = "magenta bold";
              commit = "yellow bold";
              old = "red bold";
              new = "green bold";
              whitespace = "red reverse";
            };
          };
          pull = {
            ff = "only";
          };
          init = {
            defaultBranch = "main";
          };
        };
      };
      home.packages = with pkgs.gitAndTools; [
        git
        diff-so-fancy
      ];
    };
  };
}
