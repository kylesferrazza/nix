{ lib, config, ... }:
with lib;
with lib.types;
let
  cfg = config.kyle.dunst;
in
{
  options.kyle.dunst = {
    enable = mkEnableOption "dunst";
  };
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username}.services.dunst = {
      enable = true;
      settings = {
        global = {
          font = "Fira Code 8";
          monitor = 0;
          transparency = 0;
          geometry = "300x5-30+20";
          follow = "keyboard";
          indicate_hidden = "yes";
          shrink = "no";
          notification_height = 0;
          separator_height = 2;
          padding = 8;
          horizontal_padding = 8;
          frame_width = 3;
          separator_color = "frame";
          sort = "yes";
          idle_threshold = 120;
          line_height = 0;
          markup = "full";
          format = "<b>%s</b>\\n%b";
          alignment = "left";
          show_age_threshold = 60;
          word_wrap = "yes";
          ellipsize = "middle";
          ignore_newline = "no";
          stack_duplicates = true;
          hide_duplicate_count = false;
          show_indicators = "yes";
          icon_position = "left";
          max_icon_size = 32;
          sticky_history = "yes";
          history_length = 20;
          always_run_script = true;
          title = "Dunst";
          class = "Dunst";
          startup_notification = false;
          force_xinerama = false;
        };
        shortcuts = {
          close = "ctrl+space";
          close_all = "ctrl+shift+space";
          history = "ctrl+grave";
          context = "ctrl+shift+period";
        };
        urgency_low = {
          background = "#002b36";
          foreground = "#ffffff";
          frame_color = "#2aa198";
          timeout = 10;
        };
        urgency_normal = {
          background = "#002b36";
          foreground = "#ffffff";
          frame_color = "#2aa198";
          timeout = 10;
        };
        urgency_critical = {
          background = "#002b36";
          foreground = "#ffffff";
          frame_color = "#ff0000";
          timeout = 0;
        };
      };
    };
  };
}
