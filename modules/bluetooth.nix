{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.kyle.bluetooth;
in
{
  options.kyle.bluetooth = {
    enable = mkEnableOption "Bluetooth";
  };
  config = mkIf cfg.enable {
    hardware.bluetooth.enable = true;
    services.blueman.enable = true;
    hardware.pulseaudio.package = pkgs.pulseaudioFull;
  };
}
