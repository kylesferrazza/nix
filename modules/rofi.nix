{ lib, config, ... }:
with lib;
with lib.types;
let
  cfg = config.kyle.rofi;
in
{
  options.kyle.rofi = {
    enable = mkEnableOption "rofi";
  };
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username}.programs.rofi = {
      enable = true;
      theme = "solarized";
    };
  };
}
