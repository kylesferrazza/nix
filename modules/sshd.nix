{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.sshd;
  keys = pkgs.writeText "authorized-keys" ''
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMKfa9ng8XLgftU/lzP25N7+ad19prxFKjNX99e/mJ+Y Kyle Sferrazza (gitlab.com)
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGkf0ERrifbsPUL6bt/RyeKmAhcO84TEb5QIZY7Ce6AF Kyle Sferrazza (gitlab.com)
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMq9LXisE4kyx2ZdnOJ+tbG4BC2bQt++ExW4qCR1tNUC Kyle Sferrazza (gitlab.com)
  '';
in
{
  options.kyle.sshd = {
    enable = mkEnableOption "sshd";
  };
  config = mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = [ 22 ];
    services.openssh = {
      enable = true;
      settings = {
        X11Forwarding = true;
        PermitRootLogin = lib.mkForce "no";
        PasswordAuthentication = false;
        PubkeyAuthentication = true;
      };
    };
    users.users.kyle.openssh.authorizedKeys.keyFiles = [ keys ];
  };
}
