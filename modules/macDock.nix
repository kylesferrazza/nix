{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
let
  cfg = config.kyle.dock;
  clearDock = ''
    ${pkgs.dockutil}/bin/dockutil --remove all --no-restart /Users/${config.kyle.username}
  '';
  restartDock = ''
    killall Dock
  '';
in
{
  options.kyle.dock = {
    enable = mkEnableOption "macOS dock config";
    apps = mkOption {
      type = types.listOf types.str;
      default = [ ];
    };
    others = mkOption {
      type = types.listOf types.str;
      default = [ ];
    };
  };
  config =
    let
      concatLines = lib.strings.concatStringsSep "\n";
      addDockApps = concatLines (
        map (item: "${pkgs.dockutil}/bin/dockutil --no-restart -a '${item}' -s apps /Users/${config.kyle.username}") cfg.apps
      );
      addDockOthers = concatLines (
        map (item: "${pkgs.dockutil}/bin/dockutil --no-restart -a '${item}' -s others /Users/${config.kyle.username}") cfg.others
      );
      activationScriptText = ''
        echo "clearing dock..."
        ${clearDock}
        echo "adding dock apps..."
        ${addDockApps}
        ${addDockOthers}
        echo "restarting dock..."
        ${restartDock}
      '';
    in
    mkIf cfg.enable { system.activationScripts.postActivation.text = mkAfter activationScriptText; };
}
