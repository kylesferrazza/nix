{
  lib,
  config,
  pkgs,
  extras,
  util,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.hyprland;
in
{
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ libappindicator-gtk3 ];
    programs.nm-applet.enable = true;

    home-manager.users.${config.kyle.username} = {
      xdg.configFile."hypr/hyprland.conf".text = ''
        exec-once = waybar
      '';

      programs.waybar = {
        enable = true;
        style = ''
          * {
            font-family: Iosevka;
          }

          #workspaces button.active {
            background-color: ${config.kyle.colors.cyan.light};
          }

          .module {
            margin-left: 5px;
            margin-right: 5px;
            border-bottom: 2px solid white;
          }
        '';
        settings = {
          mainBar = {
            layer = "top";
            modules-left = [ "hyprland/workspaces" ];
            modules-center = [ "hyprland/window" ];
            modules-right = [
              "tray"
              "backlight"
              "pulseaudio"
              "battery"
              "network"
              "clock"
            ];
            "hyprland/workspaces" = {
              format = "{icon}";
              on-scroll-up = "hyprctl dispatch workspace e+1";
              on-scroll-down = "hyprctl dispatch workspace e-1";
            };
            "hyprland/window" = {
              max-length = 200;
              separate-outputs = true;
            };
            battery = {
              format = "{capacity}% {icon}";
              format-icons = [
                ""
                ""
                ""
                ""
                ""
              ];
            };
            clock = {
              format = "{:%Y-%m-%d %H:%M}";
            };
            network = {
              format = "{ifname}: {ipaddr}/{cidr}";
            };
          };
        };
      };
    };
  };
}
