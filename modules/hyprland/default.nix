{
  lib,
  config,
  pkgs,
  extras,
  util,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.hyprland;
  screenclip = pkgs.callPackage ./screenclip.nix { };
in
{
  imports = [
    ./waybar.nix
    ./monitors.nix
  ];
  options.kyle.hyprland = {
    enable = mkEnableOption "hyprland";
  };
  # TODO scroll direction
  config = mkIf cfg.enable {
    kyle.wayland.enable = true;
    xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-hyprland ];
    programs.hyprland = {
      enable = true;
    };
    environment.sessionVariables.WLR_NO_HARDWARE_CURSORS = "1";
    environment.systemPackages = with pkgs; [
      wofi
      wl-clipboard
      hyprpicker
      hyprlock
    ];
    services.xserver = {
      enable = true;
      displayManager.gdm = {
        # TODO lighter weight displaymanager with theming
        enable = true;
        wayland = true;
      };
    };
    home-manager.users.${config.kyle.username} = {
      xdg.configFile."hypr/hyprpaper.conf".text = ''
        preload = ${util.bg}
        wallpaper = ,${util.bg}
        splash = false
      '';

      xdg.configFile."xdg-desktop-portal/hyprland-portals.conf".text = ''
        [preferred]
        default=hyprland;gtk
      '';
      xdg.configFile."hypr/hyprlock.conf".text = ''
        general {
          hide_cursor = true
        }

        background {
          monitor =
          path = ${util.bg}
        }

        input-field {
          monitor =
          size = 400, 50
          outline_thickness = 3
          dots_size = 0.33 # Scale of input-field height, 0.2 - 0.8
          dots_spacing = 0.15 # Scale of dots' absolute size, 0.0 - 1.0
          dots_center = false
          dots_rounding = -1 # -1 default circle, -2 follow input-field rounding
          outer_color = rgb(151515)
          inner_color = rgb(200, 200, 200)
          font_color = rgb(10, 10, 10)
          fade_on_empty = true
          fade_timeout = 1000 # Milliseconds before fade_on_empty is triggered.
          placeholder_text = <i>Input Password...</i> # Text rendered in the input box when it's empty.
          hide_input = false
          rounding = -1 # -1 means complete rounding (circle/oval)
          check_color = rgb(204, 136, 34)
          fail_color = rgb(204, 34, 34) # if authentication failed, changes outer_color and fail message color
          fail_text = <i>$FAIL <b>($ATTEMPTS)</b></i> # can be set to empty
          fail_transition = 300 # transition time in ms between normal outer_color and fail_color
          capslock_color = -1
          numlock_color = -1
          bothlock_color = -1 # when both locks are active. -1 means don't change outer color (same for above)
          invert_numlock = false # change color if numlock is off
          swap_font_color = false # see below

          position = 0, 0
          halign = center
          valign = center
        }
      '';
      xdg.configFile."hypr/hyprland.conf".text = ''
        ################
        ### MONITORS ###
        ################

        # See https://wiki.hyprland.org/Configuring/Monitors/
        # monitor=,preferred,auto,auto
        monitor = Unknown-1,disable

        ###################
        ### MY PROGRAMS ###
        ###################

        # See https://wiki.hyprland.org/Configuring/Keywords/

        # Set programs that you use
        # $terminal = kitty
        # $fileManager = dolphin
        # $menu = wofi --show drun

        #################
        ### AUTOSTART ###
        #################

        # Autostart necessary processes (like notifications daemons, status bars, etc.)
        # Or execute your favorite apps at launch like this:

        # exec-once = $terminal
        exec-once = ${pkgs.networkmanagerapplet}/bin/nm-applet &
        # exec-once = waybar & hyprpaper & firefox

        exec-once = ${pkgs.lxqt.lxqt-policykit}/bin/lxqt-policykit-agent
        exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
        exec-once = ${pkgs.hyprpaper}/bin/hyprpaper


        #############################
        ### ENVIRONMENT VARIABLES ###
        #############################

        # See https://wiki.hyprland.org/Configuring/Environment-variables/

        env = XCURSOR_SIZE,24
        env = HYPRCURSOR_SIZE,24


        #####################
        ### LOOK AND FEEL ###
        #####################

        # Refer to https://wiki.hyprland.org/Configuring/Variables/

        # https://wiki.hyprland.org/Configuring/Variables/#general
        general {
            gaps_in = 5
            gaps_out = 20

            border_size = 2

            # https://wiki.hyprland.org/Configuring/Variables/#variable-types for info about colors
            col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
            col.inactive_border = rgba(595959aa)

            # Set to true enable resizing windows by clicking and dragging on borders and gaps
            resize_on_border = false

            # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
            allow_tearing = true

            layout = dwindle
        }

        # https://wiki.hyprland.org/Configuring/Variables/#decoration
        decoration {
            rounding = 10

            # Change transparency of focused and unfocused windows
            active_opacity = 1.0
            inactive_opacity = 1.0

            shadow {
              enabled = true
              range = 4
              render_power = 3
              color = rgba(1a1a1aee)
            }

            # https://wiki.hyprland.org/Configuring/Variables/#blur
            blur {
                enabled = true
                size = 3
                passes = 1

                vibrancy = 0.1696
            }
        }

        # https://wiki.hyprland.org/Configuring/Variables/#animations
        animations {
            enabled = true

            # Default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

            bezier = myBezier, 0.05, 0.9, 0.1, 1.05

            animation = windows, 1, 7, myBezier
            animation = windowsOut, 1, 7, default, popin 80%
            animation = border, 1, 10, default
            animation = borderangle, 1, 8, default
            animation = fade, 1, 7, default
            animation = workspaces, 1, 6, default
        }

        # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
        dwindle {
            pseudotile = true # Master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
            preserve_split = true # You probably want this
        }

        # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
        master {
            new_status = master
        }

        # https://wiki.hyprland.org/Configuring/Variables/#misc
        misc {
            force_default_wallpaper = 0
            disable_hyprland_logo = true
        }


        #############
        ### INPUT ###
        #############

        # https://wiki.hyprland.org/Configuring/Variables/#input
        input {
            kb_layout = us
            kb_variant =
            kb_model =
            kb_options = caps:backspace
            kb_rules =

            follow_mouse = 0
            float_switch_override_focus = 0

            sensitivity = 0 # -1.0 - 1.0, 0 means no modification.

            touchpad {
                natural_scroll = false
            }
        }

        # https://wiki.hyprland.org/Configuring/Variables/#gestures
        gestures {
            workspace_swipe = false
        }

        # Example per-device config
        # See https://wiki.hyprland.org/Configuring/Keywords/#per-device-input-configs for more
        device {
            name = epic-mouse-v1
            sensitivity = -0.5
        }

        ####################
        ### KEYBINDINGSS ###
        ####################

        # See https://wiki.hyprland.org/Configuring/Keywords/
        $mainMod = SUPER # Sets "Windows" key as main modifier

        # Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
        bind = $mainMod, backslash, exec, ${config.kyle.browser.cmd}
        bind = $mainMod, return, exec, kitty
        bind = $mainMod, Q, killactive,
        bind = $mainMod SHIFT, Q, exit,
        bind = $mainMod, backspace, exec, ${pkgs.xfce.thunar-bare}/bin/thunar
        bind = $mainMod, D, exec, wofi --show drun --normal-window
        bind = $mainMod, minus, exec, hyprpicker -f hex -a
        bind = $mainMod, grave, exec, hyprlock
        bind = $mainMod, equal, exec, ${screenclip}/bin/screenclip
        bind = $mainMod SHIFT, equal, exec, ${screenclip}/bin/screenclip -file
        bind = $mainMod CONTROL SHIFT, equal, exec, ${screenclip}/bin/screenclip -swappy
        bind = $mainMod SHIFT, tab, togglegroup
        bind = $mainMod, tab, changegroupactive

        bind = $mainMod, F, fullscreen

        bind = $mainMod, U, layoutmsg, togglesplit

        bind = $mainMod, space, focusurgentorlast
        bind = $mainMod SHIFT, space, togglefloating

        # Move focus with mainMod + vim keys
        bind = $mainMod, h, movefocus, l
        bind = $mainMod, l, movefocus, r
        bind = $mainMod, k, movefocus, u
        bind = $mainMod, j, movefocus, d

        # Move windows with mainMod + Shift + vim keys
        bind = $mainMod SHIFT, h, movewindoworgroup, l
        bind = $mainMod SHIFT, l, movewindoworgroup, r
        bind = $mainMod SHIFT, k, movewindoworgroup, u
        bind = $mainMod SHIFT, j, movewindoworgroup, d

        # Switch workspaces with mainMod + [0-9]
        bind = $mainMod, 1, workspace, 1
        bind = $mainMod, 2, workspace, 2
        bind = $mainMod, 3, workspace, 3
        bind = $mainMod, 4, workspace, 4
        bind = $mainMod, 5, workspace, 5
        bind = $mainMod, 6, workspace, 6
        bind = $mainMod, 7, workspace, 7
        bind = $mainMod, 8, workspace, 8
        bind = $mainMod, 9, workspace, 9
        bind = $mainMod, 0, workspace, 10

        # Move active window to a workspace with mainMod + SHIFT + [0-9]
        bind = $mainMod SHIFT, 1, movetoworkspace, 1
        bind = $mainMod SHIFT, 2, movetoworkspace, 2
        bind = $mainMod SHIFT, 3, movetoworkspace, 3
        bind = $mainMod SHIFT, 4, movetoworkspace, 4
        bind = $mainMod SHIFT, 5, movetoworkspace, 5
        bind = $mainMod SHIFT, 6, movetoworkspace, 6
        bind = $mainMod SHIFT, 7, movetoworkspace, 7
        bind = $mainMod SHIFT, 8, movetoworkspace, 8
        bind = $mainMod SHIFT, 9, movetoworkspace, 9
        bind = $mainMod SHIFT, 0, movetoworkspace, 10

        # Scroll through existing workspaces with mainMod + scroll
        bind = $mainMod, mouse_down, workspace, e+1
        bind = $mainMod, mouse_up, workspace, e-1

        # Move/resize windows with mainMod + LMB/RMB and dragging
        bindm = $mainMod, mouse:272, movewindow
        bindm = $mainMod, mouse:273, resizewindow


        ##############################
        ### WINDOWS AND WORKSPACES ###
        ##############################

        # See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
        # See https://wiki.hyprland.org/Configuring/Workspace-Rules/ for workspace rules

        # Example windowrule v1
        # windowrule = float, ^(kitty)$

        # Example windowrule v2
        # windowrulev2 = float,class:^(kitty)$,title:^(kitty)$

        # windowrulev2 = suppressevent maximize, class:.* # You'll probably like this.
        # windowrulev2 = stayfocused,class:^(1Password)$
      '';
    };

    security.polkit.enable = true;
  };
}
