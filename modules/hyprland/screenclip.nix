{
  writeScriptBin,
  coreutils,
  grim,
  slurp,
  wl-clipboard,
  utillinux,
  libnotify,
  swappy,
}:
writeScriptBin "screenclip" ''
  TMP=$(${coreutils}/bin/mktemp --suffix=.png)
  ${grim}/bin/grim -g "$(${slurp}/bin/slurp -d)" "$TMP"
  if [ "$1" = "-file" ]; then
    UUIDGEN="$(${utillinux}/bin/uuidgen -t)"
    SAVEPATH="$HOME/Downloads/$UUIDGEN.png"
    echo "$SAVEPATH" | ${wl-clipboard}/bin/wl-copy
    ${libnotify}/bin/notify-send -i "$TMP" "Screenshot path copied to clipboard."
    mv "$TMP" "$SAVEPATH"
  elif [ "$1" = "-swappy" ]; then
    UUIDGEN="$(${utillinux}/bin/uuidgen -t)"
    SAVEPATH="$HOME/Downloads/$UUIDGEN.png"
    mv "$TMP" "$SAVEPATH"
    ${swappy}/bin/swappy -f $SAVEPATH
  else
    cat "$TMP" | ${wl-clipboard}/bin/wl-copy -t image/png
    ${libnotify}/bin/notify-send -i "$TMP" "Screenshot copied to clipboard."
    rm -f $TMP
  fi
''
