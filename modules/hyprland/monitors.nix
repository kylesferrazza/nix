{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.hyprland.dualMonitor;
  monitorOptions = {
    name = mkOption { type = str; };
    resolution = mkOption { type = str; };
    refresh = mkOption { type = str; };
    position = mkOption { type = str; };
  };
  genMonitorLine =
    monCfg: "monitor = ${monCfg.name},${monCfg.resolution}@${monCfg.refresh},${monCfg.position},1";
in
{
  options.kyle.hyprland.dualMonitor = {
    enable = mkEnableOption "dualMonitor";
    left = monitorOptions;
    right = monitorOptions;
  };

  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username}.xdg.configFile."hypr/hyprland.conf".text = ''
      ${genMonitorLine cfg.left}
      ${genMonitorLine cfg.right}

      workspace=1,monitor:${cfg.left.name}
      workspace=2,monitor:${cfg.right.name}
    '';
  };
}
