{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
with lib.types;
let
  cfg = config.kyle.terminal.kitty;
  faVer = lib.last (lib.strings.split "-" pkgs.font-awesome.name);
  fontAwesomeVersion = lib.head (lib.strings.split "\\." faVer);
in
{
  options.kyle.terminal.kitty = {
    enable = mkEnableOption "kitty";
  };
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username} = {
      home.sessionVariables.TERMINAL = "kitty";

      programs.kitty = {
        enable = true;

        extraConfig = ''
          font_family Iosevka
          italic_font Iosevka Italic
          bold_font Iosevka Bold
          bold_italic_font Iosevka Bold Italic

          symbol_map U+f000-U+f941 Font Awesome ${fontAwesomeVersion} Free Solid
          disable_ligatures always
        '';

        settings = {
          font_size = "10.0";

          adjust_line_height = "0";
          adjust_column_width = "0";

          box_drawing_scale = "0.001, 1, 1.5, 2";

          cursor = config.kyle.colors.cursor;
          cursor_shape = "beam";
          cursor_blink_interval = "0.5";
          cursor_stop_blinking_after = "15.0";

          scrollback_lines = "2000";
          scrollback_pager = "less +G -R";

          wheel_scroll_multiplier = "5.0";

          url_color = "#0087BD";
          url_style = "curly";
          open_url_modifiers = "ctrl+shift";
          open_url_with = "default";

          copy_on_select = false;
          rectangle_select_modifiers = "ctrl+alt";
          select_by_word_characters = ":@-./_~?&=%+#";
          click_interval = "0.5";
          mouse_hide_wait = "3.0";
          focus_follows_mouse = false;

          repaint_delay = "10";
          input_delay = "3";
          sync_to_monitor = true;

          visual_bell_duration = "0.0";
          enable_audio_bell = false;

          enabled_layouts = "*";

          window_border_width = "1";
          window_margin_width = "0";
          window_padding_width = "0";
          active_border_color = "#00ff00";
          inactive_border_color = "#cccccc";
          inactive_text_alpha = "1.0";

          tab_bar_edge = "bottom";
          tab_separator = " ┇";
          active_tab_foreground = "#000";
          active_tab_background = "#eee";
          active_tab_font_style = "bold-italic";
          inactive_tab_foreground = "#444";
          inactive_tab_background = "#999";
          inactive_tab_font_style = "normal";

          foreground = config.kyle.colors.foreground;
          background = config.kyle.colors.background;
          background_opacity = "1.0";
          selection_foreground = config.kyle.colors.background;
          selection_background = config.kyle.colors.foreground;

          color0 = config.kyle.colors.black.dark;
          color8 = config.kyle.colors.black.light;

          # red dark/light
          color1 = config.kyle.colors.red.dark;
          color9 = config.kyle.colors.red.light;

          # green dark/light
          color2 = config.kyle.colors.green.dark;
          color10 = config.kyle.colors.green.light;

          # yellow dark/light
          color3 = config.kyle.colors.yellow.dark;
          color11 = config.kyle.colors.yellow.light;

          # blue dark/light
          color4 = config.kyle.colors.blue.dark;
          color12 = config.kyle.colors.blue.light;

          # magenta dark/light
          color5 = config.kyle.colors.magenta.dark;
          color13 = config.kyle.colors.magenta.light;

          # cyan dark/light
          color6 = config.kyle.colors.cyan.dark;
          color14 = config.kyle.colors.cyan.light;

          # white dark/light
          color7 = config.kyle.colors.white.dark;
          color15 = config.kyle.colors.white.light;

          # shell = "fish";
          close_on_child_death = true;
          allow_remote_control = false;
          term = "xterm-256color";

          macos_titlebar_color = "system";
          hide_window_decorations = false;
          macos_option_as_alt = true;
        };

        keybindings = {
          "ctrl+shift+v" = "paste_from_clipboard";
          "ctrl+shift+s" = "paste_from_selection";
          "ctrl+shift+c" = "copy_to_clipboard";
          "shift+insert" = "paste_from_selection";
          "ctrl+shift+o" = "pass_selection_to_program";

          "ctrl+shift+up" = "scroll_line_up";
          "ctrl+shift+down" = "scroll_line_down";
          "ctrl+shift+k" = "scroll_line_up";
          "ctrl+shift+j" = "scroll_line_down";
          "ctrl+shift+page_up" = "scroll_page_up";
          "ctrl+shift+page_down" = "scroll_page_down";
          "ctrl+shift+home" = "scroll_home";
          "ctrl+shift+end" = "scroll_end";
          "ctrl+shift+h" = "show_scrollback";

          "ctrl+shift+enter" = "new_window";
          "ctrl+shift+n" = "new_os_window";
          "ctrl+shift+w" = "close_window";
          "ctrl+shift+]" = "next_window";
          "ctrl+shift+[" = "previous_window";
          "ctrl+shift+f" = "move_window_forward";
          "ctrl+shift+b" = "move_window_backward";
          "ctrl+shift+`" = "move_window_to_top";
          "ctrl+shift+1" = "first_window";
          "ctrl+shift+2" = "second_window";
          "ctrl+shift+3" = "third_window";
          "ctrl+shift+4" = "fourth_window";
          "ctrl+shift+5" = "fifth_window";
          "ctrl+shift+6" = "sixth_window";
          "ctrl+shift+7" = "seventh_window";
          "ctrl+shift+8" = "eighth_window";
          "ctrl+shift+9" = "ninth_window";
          "ctrl+shift+0" = "tenth_window";
          "ctrl+alt+enter" = "new_os_window_with_cwd";

          "ctrl+shift+right" = "next_tab";
          "ctrl+shift+left" = "previous_tab";
          "ctrl+shift+t" = "new_tab";
          "ctrl+shift+q" = "close_tab";
          "ctrl+shift+l" = "next_layout";
          "ctrl+shift+." = "move_tab_forward";
          "ctrl+shift+," = "move_tab_backward";

          "ctrl+shift+equal" = "increase_font_size";
          "ctrl+shift+minus" = "decrease_font_size";
          "ctrl+shift+backspace" = "restore_font_size";

          "ctrl+shift+f11" = "toggle_fullscreen";
          "ctrl+shift+u" = "input_unicode_character";
          "ctrl+shift+f2" = "edit_config_file";

          "ctrl+shift+e" = "run_simple_kitten text url_hints";
        };
      };
    };
  };
}
