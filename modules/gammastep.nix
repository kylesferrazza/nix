{ lib, config, ... }:
with lib;
with lib.types;
let
  cfg = config.kyle.gammastep;
in
{
  options.kyle.gammastep = {
    enable = mkEnableOption "gammastep";
  };
  config = mkIf cfg.enable {
    home-manager.users.${config.kyle.username}.services.gammastep = {
      enable = true;
      latitude = "40";
      longitude = "-74";
      temperature = {
        day = 5500;
        night = 3500;
      };
      tray = true;
    };
  };
}
