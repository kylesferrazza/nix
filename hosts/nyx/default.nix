{ pkgs, lib, ... }:
{
  imports = [
    ../../roles/standard
    ../../roles/graphical
    # ../../roles/laptop
    ../../arch/intel
  ];

  networking.hostName = "nyx";
  kyle = {
    hidpi.enable = true;
    sshd.enable = true;
    colors.solarized-dark.enable = true;
    gtk.solarized-dark.enable = true;
    # browser.cmd = "echo";
  };

  # virtualisation.vmware.host.enable = true;
}
