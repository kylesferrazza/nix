{ modulesPath, lib, ... }:
{
  imports = [
    "${modulesPath}/installer/scan/not-detected.nix"
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  # kyle.efi.enable = true;
  boot.initrd.availableKernelModules = [
    "ata_piix"
    "uhci_hcd"
    "virtio_pci"
    "virtio_scsi"
    "sd_mod"
    "sr_mod"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/ba857597-860b-4488-a8e2-03cd6ed07d75";
    fsType = "ext4";
  };

  swapDevices = [ ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;
  networking.hostName = "nyx"; # Define your hostname.

  networking.useDHCP = lib.mkDefault true;
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}
