{ modulesPath, lib, ... }:
{
  imports = [ "${modulesPath}/installer/scan/not-detected.nix" ];

  kyle.efi.enable = true;
  # TODO kyle.amd.enable = true; # instead of import in default.nix
  kyle.nvidia.enable = true;

  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "nvme"
    "usbhid"
  ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];

  boot.initrd.luks.devices.rt.device = "/dev/disk/by-uuid/393106d7-fd7f-41f2-ace2-1bb77bbe4505";

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/cb568625-1a31-4a53-a374-aad96e1d028b";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/CEB8-3C3E";
    fsType = "vfat";
    options = [
      "fmask=0022"
      "dmask=0022"
    ];
  };

  swapDevices = [ { device = "/dev/disk/by-uuid/5833c0a0-9d94-4a7a-908e-ca3fba540d8c"; } ];

  nix.settings.max-jobs = 16;
  nixpkgs.hostPlatform = "x86_64-linux";
}
