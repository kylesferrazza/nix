{ pkgs, ... }:
{
  boot.initrd.availableKernelModules = [ "vfio-pci" ];
  boot.initrd.preDeviceCommands = ''
    DEVS="0000:03:00.0 0000:03:00.1"
    for DEV in $DEVS; do
      echo "vfio-pci" > /sys/bus/pci/devices/$DEV/driver_override
    done
    modprobe -i vfio-pci
  '';
  boot.kernelParams = [
    "amd_iommu=on"
    "pcie_aspm=off"
  ];
  boot.kernelModules = [ "kvm-amd" ];
}
