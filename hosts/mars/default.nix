{ config, lib, ... }:
{
  imports = [
    ../../roles/standard
    ../../roles/graphical
    # ./vfio.nix
    ../../arch/amd
  ];

  networking.hostName = "mars";
  networking.nameservers = lib.mkBefore [ "10.99.0.1" ];

  hardware.keyboard.zsa.enable = true;

  # kyle.hidpi.enable = true;

  kyle.sshd.enable = true;
  kyle.bluetooth.enable = true;

  home-manager.users.${config.kyle.username}.xdg.configFile."hypr/hyprland.conf".text = ''
    monitor=DP-1, 2560x1440@144, 0x0, 1
    monitor=HDMI-A-1, 2560x1440@144, 2560x0, 1
  '';

  # services.zerotierone.enable = true;
  # services.cloudflare-warp.enable = true;

  # TODO combine dualMonitor modules

  kyle.hyprland.dualMonitor = {
    enable = true;
    left = {
      name = "DP-1";
      resolution = "2560x1440";
      refresh = "144";
      position = "0x0";
    };
    right = {
      name = "HDMI-A-1";
      resolution = "2560x1440";
      refresh = "144";
      position = "2560x0";
    };
  };

  kyle.i3 = {
    dualMonitor = {
      enable = true;
      left = {
        name = "DP-0";
        resolution = "2560x1440";
        refresh = "120";
      };
      right = {
        name = "HDMI-0";
        resolution = "2560x1440";
        refresh = "120";
      };
    };
  };
}
