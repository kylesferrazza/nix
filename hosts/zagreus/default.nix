{ pkgs, lib, ... }:
{
  imports = [
    ../../roles/standard
    ../../roles/graphical
    ../../roles/laptop
    ../../arch/intel
  ];

  networking.hostName = "zagreus";
  kyle.hidpi.enable = true;

  # virtualisation.vmware.host.enable = true;
}
