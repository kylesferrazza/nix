{ modulesPath, lib, ... }:
{
  imports = [ "${modulesPath}/installer/scan/not-detected.nix" ];

  kyle.efi.enable = true;
  # TODO kyle.intel.enable = true; # instead of import in default.nix
  kyle.nvidia.enable = true;

  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "ahci"
    "nvme"
    "usb_storage"
    "sd_mod"
    "rtsx_pci_sdmmc"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/a21efb29-81bc-4eb4-9a35-9f2a3a8c6e5e";
    fsType = "ext4";
  };

  boot.initrd.luks.devices."luks-6b713f09-9921-479f-bfe3-22d91b724704".device =
    "/dev/disk/by-uuid/6b713f09-9921-479f-bfe3-22d91b724704";
  boot.initrd.luks.devices."luks-4ca7530e-a191-495c-95b3-15ee9c176fbb".device =
    "/dev/disk/by-uuid/4ca7530e-a191-495c-95b3-15ee9c176fbb";

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/D4AE-3073";
    fsType = "vfat";
    options = [
      "fmask=0022"
      "dmask=0022"
    ];
  };

  swapDevices = [ { device = "/dev/disk/by-uuid/4d83e159-662f-44e9-b4d1-a757660c7edc"; } ];

  nix.settings.max-jobs = 16;
  nixpkgs.hostPlatform = "x86_64-linux";
}
