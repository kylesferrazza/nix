{
  modulesPath,
  lib,
  pkgs,
  ...
}:
{
  imports = [ "${modulesPath}/installer/scan/not-detected.nix" ];

  config = lib.mkDefault {
    boot = {
      kernelParams = [ "cma=32M" ];
      loader = {
        grub.enable = false;
        raspberryPi = {
          enable = true;
          version = 3;
          uboot.enable = true;
        };
        timeout = 1;
      };
      cleanTmpDir = true;
    };

    fileSystems = {
      "/" = {
        device = "/dev/disk/by-label/NIXOS_SD";
        fsType = "ext4";
      };
    };

    swapDevices = [
      {
        device = "/swapfile";
        size = 1024;
      }
    ];

    nix.gc = {
      automatic = true;
      options = "--delete-older-than 10d";
    };

    nix.settings.max-jobs = 4;
    powerManagement.cpuFreqGovernor = "ondemand";
  };
}
