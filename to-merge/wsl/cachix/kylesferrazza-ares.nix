{
  nix = {
    settings = {
      substituters = [ "https://kylesferrazza-ares.cachix.org" ];
      trusted-public-keys = [
        "kylesferrazza-ares.cachix.org-1:KK6R6nLWQLke1vkYKDA5E2BD6gyfhvvyUZ8KeVvHy58="
      ];
    };
  };
}
