{
  lib,
  nixos-wsl,
  pkgs,
  config,
  neovim-config,
  ...
}:
{
  imports = [
    nixos-wsl.nixosModules.wsl
    ./cachix.nix
    ./systemd-fix.nix
  ];

  networking.hostName = "ares-wsl";

  wsl = {
    enable = true;
    wslConf.automount.root = "/mnt";
    defaultUser = "kyle";
    startMenuLaunchers = true;

    # nativeSystemd = true;

    # Enable native Docker support
    # docker-native.enable = true;

    # Enable integration with Docker Desktop (needs to be installed)
    # docker-desktop.enable = true;

    extraBin = with pkgs; [
      # Binaries for Docker Desktop wsl-distro-proxy
      { src = "${coreutils}/bin/mkdir"; }
      { src = "${coreutils}/bin/cat"; }
      { src = "${coreutils}/bin/whoami"; }
      { src = "${coreutils}/bin/ls"; }
      { src = "${busybox}/bin/addgroup"; }
      { src = "${su}/bin/groupadd"; }
      { src = "${su}/bin/usermod"; }
    ];
  };

  virtualisation.docker = {
    enable = true;
    enableOnBoot = true;
    autoPrune.enable = true;
  };

  ## patch the script
  systemd.services.docker-desktop-proxy.script = lib.mkForce ''${config.wsl.wslConf.automount.root}/wsl/docker-desktop/docker-desktop-user-distro proxy --docker-desktop-root ${config.wsl.wslConf.automount.root}/wsl/docker-desktop "C:\Program Files\Docker\Docker\resources"'';

  # https://gitlab.com/kylesferrazza/nix-configs/-/jobs/8082986446#L2269
  # https://discourse.nixos.org/t/logrotate-config-fails-due-to-missing-group-30000/28501/7
  services.logrotate.checkConfig = false;

  users.users.kyle.extraGroups = [ "docker" ];

  environment.systemPackages = with pkgs; [
    zellij
    nodePackages.typescript-language-server
    talosctl
    jujutsu
    yaml-language-server
    nil
    tmux
    wget
    cachix
    direnv
    neovim-config.defaultPackage.x86_64-linux
    ripgrep
    (pkgs.writeShellScriptBin "wslpath" ''
      /bin/wslpath "$@"
    '')
    file
    fzf
    fishPlugins.fzf-fish
    kubectl
    kubevirt
    nodejs
    k9s
    socat
    _1password-cli
    (pkgs.writeShellScriptBin "1p-fix" ''
      rm /home/kyle/.1password/agent.sock
      socat UNIX-LISTEN:$SSH_AUTH_SOCK,fork EXEC:"/mnt/s/Documents/bin/npiperelay.exe -ei -s //./pipe/openssh-ssh-agent",nofork &
      disown
    '')
  ];

  home-manager.users.${config.kyle.username} = {
    home.stateVersion = "24.05";

    kyle = {
      helix = {
        enable = true;
        default = true;
      };
    };

    home.file.".vscode-server/server-env-setup" = {
      executable = true;
      text = ''
        #!/usr/bin/env bash

        source /etc/bashrc

        echo "== '~/.vscode-server/server-env-setup' SCRIPT START =="

        VSCODE_SERVER_DIR="$HOME/.vscode-server"
        echo "Got vscode directory : $VSCODE_SERVER_DIR"

        echo "Overwriting nodejs binaries..."
        nix shell nixpkgs#nodejs-18_x -c bash -c "
            for versiondir in $VSCODE_SERVER_DIR/bin/*/; do
                ln -vsf \"\$(which node)\" \"\$versiondir\"\"node\"
            done
        "

        echo "== '~/.vscode-server/server-env-setup' SCRIPT END =="
      '';
    };
  };

  services.openssh = {
    enable = true;
    ports = [ 2222 ];
    settings = {
      PasswordAuthentication = false;
    };
  };

  users.users.kyle.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP6NyH+h7cSi1JUJEQZVwtqubaQLUf9dytdE8jr3dk3D kyle@ares"
  ];

  environment.sessionVariables = {
    EDITOR = "hx";
  };

  fonts.packages = with pkgs; [
    emacs-all-the-icons-fonts
    (nerdfonts.override { fonts = [ "Iosevka" ]; })
  ];

  system.activationScripts.command-not-found-channel.text = ''
    ${pkgs.nix}/bin/nix-channel --update
  '';

  programs.bash.loginShellInit = ''
    # Code extracted from https://stuartleeks.com/posts/wsl-ssh-key-forward-to-windows/ with minor modifications

    # Configure ssh forwarding
    mkdir -p $HOME/.1password
    export SSH_AUTH_SOCK=$HOME/.1password/agent.sock
    # need `ps -ww` to get non-truncated command for matching
    # use square brackets to generate a regex match for the process we want but that doesn't match the grep command running it!
    ALREADY_RUNNING=$(ps -auxww | grep -q "[n]piperelay.exe -ei -s //./pipe/openssh-ssh-agent"; echo $?)
    if [[ $ALREADY_RUNNING != "0" ]]; then
        if [[ -S $SSH_AUTH_SOCK ]]; then
            # not expecting the socket to exist as the forwarding command isn't running (http://www.tldp.org/LDP/abs/html/fto.html)
            echo "removing previous socket..."
            rm $SSH_AUTH_SOCK
        fi
        echo "Starting SSH-Agent relay..."
        # setsid to force new session to keep running
        # set socat to listen on $SSH_AUTH_SOCK and forward to npiperelay which then forwards to openssh-ssh-agent on windows
        (setsid ${pkgs.socat}/bin/socat UNIX-LISTEN:$SSH_AUTH_SOCK,fork EXEC:"npiperelay.exe -ei -s //./pipe/openssh-ssh-agent",nofork &) >/dev/null 2>&1
    fi
  '';

  programs.bash.interactiveShellInit = ''
    exec fish
  '';

  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      set fish_greeting
      ${pkgs.direnv}/bin/direnv hook fish | source
    '';
  };

  programs.starship.enable = true;

  programs.git = {
    enable = true;
    config = {
      user = {
        name = "Kyle Sferrazza";
        email = "git@kylesferrazza.com";
        signingkey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMKfa9ng8XLgftU/lzP25N7+ad19prxFKjNX99e/mJ+Y";
      };
      gpg.format = "ssh";
      commit.gpgsign = "true";
      core.excludesFile = pkgs.writeText "gitignore" ''
        /.direnv/
        /.envrc
      '';
    };
  };

  time.timeZone = "America/New_York";

  nixpkgs.config.allowUnfree = true;

  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  nix.settings.trusted-users = [
    "root"
    "kyle"
  ];

  system.stateVersion = "22.11";
}
