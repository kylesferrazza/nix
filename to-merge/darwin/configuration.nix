{
  pkgs,
  lib,
  spicetify-nix,
  config,
  ...
}:
{
  system.stateVersion = 4;
  home-manager.users.${config.kyle.username} = {
    home.stateVersion = "22.11";
    home.homeDirectory = lib.mkForce "/Users/kyle";
    xdg.configFile = {
      "karabiner/karabiner.json".source = ./karabiner.json;
      "fish/config.fish".source = ./config.fish;
      "iterm2/com.googlecode.iterm2.plist".source = ./iterm2.plist;
    };
    kyle = {
      helix = {
        enable = true;
        default = true;
      };
    };
  };

  kyle = {
    colors.solarized-dark.enable = true;
    starship.enable = true;
    terminal.kitty.enable = true;
  };

  users.users.kyle = {
    name = "kyle";
    shell = pkgs.fish;
  };

  services.nix-daemon.enable = true;

  nixpkgs.config.allowUnfree = true;

  nix = {
    settings = {
      trusted-users = [
        "kyle"
        "root"
      ];
      experimental-features = [
        "nix-command"
        "flakes"
      ];
    };
  };

  environment.systemPackages = with pkgs; [
    jujutsu
    dockutil
    kubectl
    direnv
    starship
    cachix
    nixfmt-rfc-style
    ripgrep
    kubectl
    talosctl
    watch
    jq
    spicetify-nix.solarizedDark
    nil
    yaml-language-server
  ];

  programs.fish.enable = true;

  homebrew = {
    enable = true;
    brewPrefix = "/opt/homebrew/bin";
    brews = [
      "deno"
      "glab"
      "gnupg"
      "mosh"
      "spice-gtk"
      "tmux"
    ];
    masApps = {
      "Tailscale" = 1475387142;
    };
    casks = [
      "rectangle"
      "1password-cli"
      "font-iosevka"
      "karabiner-elements"
      "netnewswire"
      "discord"
      "signal"
      "utm"
      "obsidian"
      "orbstack"
    ];
  };

  system.defaults = {
    dock.autohide = true;
    finder = {
      AppleShowAllFiles = true;
      AppleShowAllExtensions = true;
      ShowPathbar = true;
      ShowStatusBar = true;
    };

    NSGlobalDomain = {
      "com.apple.swipescrolldirection" = false;
    };
  };

  kyle.dock = {
    enable = true;
    apps = [
      "/System/Applications/Messages.app"
      "/System/Applications/Calendar.app"
      "/Applications/Vivaldi.app"
      "/Applications/1Password.app"
      "/Applications/NetNewsWire.app"
      "/Applications/Signal.app"
      "/Applications/Discord.app"
      "/Applications/Nix Apps/Spotify.app"
      "/Applications/UTM.app"
      "/System/Applications/Mail.app"
      "/Applications/kitty.app"
      "/System/Applications/Notes.app"
      "/Applications/Flighty.app"
      "/System/Applications/iPhone Mirroring.app"
      "/System/Applications/Photos.app"
      "/Applications/OrbStack.app"
      "/Applications/Obsidian.app"
      "/System/Applications/System Settings.app"
    ];
    others = [ "/Users/kyle/Downloads" ];
  };
}
