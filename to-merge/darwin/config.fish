set --export --append PATH /run/current-system/sw/bin
set --export --append PATH "/Users/kyle/.rd/bin"

if status is-interactive
    # Commands to run in interactive sessions can go here
    if test -e /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.fish
        source /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.fish
    end

    export SSH_AUTH_SOCK="/Users/kyle/Library/Group Containers/2BUA8C4S2C.com.1password/t/agent.sock"
    source /Users/kyle/.config/op/plugins.sh

    direnv hook fish | source
    starship init fish | source
end
