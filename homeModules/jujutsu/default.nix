{ lib, config, ... }:
with lib;
let
  cfg = config.kyle.jujutsu;
in
{
  options.kyle.jujutsu.enable = mkEnableOption "jujutsu";
  config = mkIf cfg.enable {
    programs.jujutsu = {
      enable = true;
      settings = {
        user = {
          name = "Kyle Sferrazza";
          email = "git@kylesferrazza.com";
        };
        ui = {
          pager = "less -FRX";
          paginate = "auto";
          default-command = "log";
        };
      };
    };
  };
}
