{ lib, config, ... }:
with lib;
let
  cfg = config.kyle.helix;
in
{
  options.kyle.helix.enable = mkEnableOption "helix";
  options.kyle.helix.default = mkOption {
    type = types.bool;
    default = false;
  };
  config = mkIf cfg.enable {
    home.sessionVariables.EDITOR = mkIf cfg.default "hx";
    programs.helix = {
      enable = true;
    };
    xdg.configFile."helix/config.toml".source = ./config.toml;
    xdg.configFile."helix/languages.toml".source = ./languages.toml;
  };
}
